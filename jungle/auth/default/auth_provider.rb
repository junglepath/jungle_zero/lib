begin # requires
  require 'jungle/auth/default/password_hash'
  require 'jungle/lib/data_structures'
  require 'jungle/sql/helpers'
end

module Jungle
  module Auth
    module Default
      module AuthProvider
        begin # namespaces
          DataStructures = ::Jungle::Lib::DataStructures
          PasswordHash = ::Jungle::Auth::Default::PasswordHash
          Sql = ::Jungle::Sql::Helpers
        end
  
        def self.authenticate(username:, password:, roles:, data:)
          username = esn(username)
          password = esn(password)
          
          login = parse_login(username)
          puts "login: #{login}."

          # authenticating user:
          user, role = authenticating_user(data, login, username, password)

          # impersonation target user -- if any:
          iuser, irole = authenticating_impersonation_if_any(data, roles, user, role, login)

          authenticated = (user != nil)
          user = user || {}
          iuser = iuser || {}

          identity = {
            status: {
              authenticated: authenticated
            },
            login: {
              raw: username,
              password: password, # todo -- don't include
              parsed: login
            },
            user: {
              id: user[:id],
              username: user[:username],
              name: user[:name],
              role: role,
              roles: user[:roles]
            },
            iuser: {
              id: iuser[:id],
              username: iuser[:username],
              name: iuser[:name],
              role: irole,
              roles: iuser[:roles]
            }
          }
          DataStructures.json_puts(identity, title: 'identity:')
          identity
        end

        private

        def self.authenticating_user(data, login, username, password)
          user = authenticate_user(data, login[:user], password) # authenticated user!
          raise "authentication error: username: #{username}." unless user if (username || password)
          raise "authentication error: disabled: username: #{login[:user][:usename]}." unless user[:enabled] if user
          desired_role = login[:user][:role] || user[:default_role] if user
          role = check_role(user[:roles], desired_role) if user
          raise "authentication error: role: disabled or invalid role: #{desired_role}!" if user unless role
          return user, role
        end

        def self.authenticating_impersonation_if_any(data, roles, user, role, login)
          raise "authentication error: unauthorized! impersonation permission denied!" unless has_permission?(roles, role, one_of:[:root, :assume_user_identity]) if login[:iuser][:username]
          iuser = data[:fn_find_user].call(login[:iuser][:username]) if user # (..only if we have an authenticated user!)
          raise "authentication error: iuser:username: #{login[:iuser][:username]}" unless iuser if login[:iuser][:username]
          raise "authentication error: iuser:disabled: username: #{login[:iuser][:usename]}." unless iuser[:enabled] if iuser
          iuser[:roles] = data[:fn_find_roles].call(iuser) if iuser
          idesired_role = login[:iuser][:role] || iuser[:default_role] if iuser
          irole = check_role(iuser[:roles], idesired_role) if iuser
          raise "authentication error: irole: disabled or invalid irole: #{idesired_role} for iuser: #{iuser[:username]}!" unless irole if iuser
          raise "authentication error: unauthorized! impersonation permission denied for iuser target: #{iuser[:username]}!" unless has_permission?(roles, irole, one_of:[:assumable_user_identity]) || has_permission?(roles, role, one_of:[:root]) if iuser
          return iuser, irole
        end

        def self.has_permission?(roles, role, one_of:[])
          puts "has_permission?"
          puts "roles: #{roles}"
          puts "role: #{role}."
          r = roles[role.to_sym]
          puts "found r!" if r
          one_of.each do |permission|
            return true if r[:permissions].include? permission
          end
          false
        end

        def self.authenticate_user(data, login, password)
          user = nil
          found_user = data[:fn_find_user].call(login[:username])
          puts "found_user: #{found_user}."
          validated = PasswordHash.validate_password(password, found_user[:password_hash]) if found_user
          user = found_user if validated
          user[:roles] = data[:fn_find_roles].call(user) if user
          user
        end

        def self.parse_login(username)
          parts = username.split('|') if username
          username = esn((parts && parts[0]) || nil)
          role = esn((parts && parts[1]) || nil)
          impersonate_username = esn((parts && parts[2]) || nil)
          impersonate_role = esn((parts && parts[3]) || nil)
          login = {
            user: {
              username: username,
              role: role
            },
            iuser: {
              username: impersonate_username,
              role: impersonate_role
            }
          }
          login
        end

        def self.esn(input)
          # empty string nil
          return nil if input == nil
          s = "#{input.strip}"
          return nil if s.length == 0
          s
        end

        def self.check_role(roles, role)
          # check if role is a valid role for user
          confirmed_role = nil
          roles.each do |r|
            if r[:role] == role && r[:enabled]
              confirmed_role = role
              break
            end
          end
          confirmed_role
        end
      end
    end
  end
end
