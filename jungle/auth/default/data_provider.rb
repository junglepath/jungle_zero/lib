module Jungle
  module Auth
    module Default
      module DataProvider
        begin # namespaces
          Sql = ::Jungle::Sql::Helpers
        end

        def self.find_user(db, username)
          user = nil
          sql = Sql.load('user.sql')
          puts "username: #{username}."
          db.base['select * from "user" where username = ?', username].each do |row|
            user = {
              id: row[:id],
              name: row[:name],
              username: row[:username],
              default_role: row[:default_role],
              email: row[:email],
              enabled: row[:enabled],
              created_at: row[:created_at],
              password_hash: row[:password_hash]
            }
            break
          end if username
          user
        end
        
        def self.find_roles(db, user)
          if user
            roles = []
            sql = Sql.load('user_role.sql')
            db.base[sql, user[:id]].each do |row|
              roles << row
            end
          end
          roles
        end
      end
    end
  end
end