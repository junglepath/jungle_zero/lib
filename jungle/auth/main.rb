#begin # requires
#  require 'jungle/auth/default/provider'
#end

module Jungle
  module Auth
    module Main
      def self.authenticate(fn_auth)
        fn_auth.call()
      end
    end
  end
end
