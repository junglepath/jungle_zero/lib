# config.rb

module Jungle
  module Config
    def self.override_settings(path, hash, mod, name:'override')
      begin
        override_file = File.join(path, name)
				require_relative override_file
				puts "#{override_file}.rb file was loaded."
				hash = mod.override(hash)
			rescue LoadError => ex
				puts "warning!!! your configuration override file: #{override_file}.rb was not found."
			end
			hash
		end
  end
end
