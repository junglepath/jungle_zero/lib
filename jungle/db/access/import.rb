require_relative './import/db_dir'
require_relative './import/delete'
require_relative './import/insert'
require_relative './import/schema'
require_relative './import/select'
module Jungle
	module Db
		module Access
			module Import
			  def self.data_file_name(path, table_name)
			    File.join(path, "#{table_name}.dat")
			  end
			end
		end
	end
end
