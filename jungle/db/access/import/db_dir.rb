require_relative './delete'
require_relative './insert'
require_relative './schema'
require_relative './select'

module Jungle
	module Db
		module Access
			module Import
				class DbDir
			    attr_reader :config
			    attr_reader :path
			    attr_reader :log_path
			    attr_reader :select
			    attr_reader :insert
			    attr_reader :delete
			    attr_reader :schema
			    def initialize(config, logger=nil)
			      @logger = logger
			      @config = config
			      @path = config.import_file_path
			      @log_path = config.import_log_path
			      Dir.mkdir(path) unless Dir.exist?(path)
			      clear_import_files
			      @select = Jungle::DBAccess::Import::Select.new @config, @logger
			      @insert = Jungle::DBAccess::Import::Insert.new @config, @logger
			      @delete = Jungle::DBAccess::Import::Delete.new @config, @logger
			      @schema = Jungle::DBAccess::Import::Schema.new @config, @logger
			    end
			    def reset_sequence_for_table(table_name)
			      @insert.db.reset_sequence_for_table(table_name)
			    end
			    def transaction
			      begin_transaction
			      yield
			      commit_transaction
			    rescue
			      rollback_transaction
			      raise
			    end
			    def clear_import_files
			      if Dir.exist?(path)
			        data_files = Dir.glob(File.join(path, "*.dat"))
			        data_files.each do |file_name|
			          File.delete file_name
			        end
			      end
			    end
			    private
			    def begin_transaction
			      # not implemented :)
			    end
			    def commit_transaction
			      @insert.close_files
			      @insert.write_to_db
			      @insert.reset
			    end
			    def rollback_transaction
			      @insert.close_files
			      @insert.reset
			    end
				end
		  end
		end
	end
end
