require_relative '../io/db'
module Jungle
	module Db
		module Access
			module Import
				class Delete
					def initialize(config, logger=nil)
						@logger = logger
						@path = config.import_file_path
					end
					def _models(model, force=false)
						if force
							file_name = Jungle::Db::Access::Import.data_file_name(@path, model.class.table_name)
							File.delete file_name if File.exist?(file_name)
						end
					end
				end
			end
		end
	end
end
