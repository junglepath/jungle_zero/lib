require_relative '../io/db'
require_relative '../import'
module Jungle
	module Db
		module Access
			module Import
				class Insert
					attr_reader :db
					def initialize(config, logger=nil)
						@logger = logger
						@path = config.import_file_path
						@db = Jungle::Db::Access::Io::Db.new(config, logger)
						reset
					end
					def _model(model)
						table_name = model.class.table_name
						file = open_file(model)
						@counters[table_name] += 1
						if model.respond_to?(:id) and model.id == nil
							model.id = @counters[table_name]
						end
						file.puts model_values_to_string(model)
						puts "    #{table_name} rows processed count: #{@counters[table_name]}." if @counters[table_name] % 1000 == 0
						model
					end
					def open_file(model)
						table_name = model.class.table_name
						file = @files[table_name]
						unless file
							file = File.open(Jungle::Db::Access::Import.data_file_name(@path, table_name), 'a')
							@models[table_name] = model
							@files[table_name] = file
							@counters[table_name] = 0
							puts "writing file: #{table_name}."
						end
						file
					end
					def close_files
						@files.each do |key, file|
							unless file.closed?
								file.fsync
								file.close
								puts "    #{key} rows processed count: #{@counters[key]}."
							end
						end
					end
					def reset
						@models = {}
						@files = {}
						@counters = {}
					end
					def write_to_db
						if @db
							@models.each do |key, model|
								file_name = Jungle::Db::Access::Import.data_file_name(@path, model.class.table_name)
								@db.copy.into_table model.class, file_name
								File.delete file_name
							end
						end
					end
					def model_values_to_string(model)
						values = []
						model._columns.each do |key, column|
							value = model._values[key]
							if column.base_type == :json or column.base_type == :jsonb
								values << json_dump(value)
							else
								values << value_to_string(value)
							end
						end
						values.join("\t")
					end
					def string_fix(value)
						v = value.gsub('\\', '\\\\\\\\')
						v = v.gsub('"', '\\\\\"')
						v = v.gsub("\n", '\n')
						v = v.gsub("\r", '\r')
						v = v.gsub("\t", '\t')
					end
					def json_dump(value)
						if value.class == Hash
							json_dump_hash(value)
						elsif value.class == Array
							json_dump_array(value)
						else
							value_to_string(value, true)
						end
					end
					def json_dump_hash(hash)
						a = []
						hash.each do |key, value|
							if value.class == Hash
								#a << json_dump_hash(value)
								a << "\"#{key}\":#{json_dump_hash(value)}"
							elsif value.class == Array
								#a << json_dump_array(value)
								a << "\"#{key}\":#{json_dump_array(value)}"
							else
								a << "\"#{key}\":#{value_to_string(value, true)}"
							end
						end
						"{#{a.join(',')}}"
					end
					def json_dump_array(array)
						values = []
						array.each do |value|
							if value.class == Hash
								#a << json_dump_hash(value)
								a << "\"#{key}\":#{json_dump_hash(value)}"
							elsif value.class == Array
								#a << json_dump_array(value)
								a << "\"#{key}\":#{json_dump_array(value)}"
							else
								values << "#{value_to_string(value, true)}"
							end
						end
						"[#{values.join(',')}]"
					end
					def value_to_string value, json=false
						v = nil
						if value == nil
							v = '\N'
						elsif value.class == String
							v = string_fix(value)
							v = "\"#{v}\"" if json
						elsif value.class == DateTime
							v = value.strftime("%Y-%m-%d %H:%M:%S.%6N")
							v = "\"#{v}\"" if json
						elsif value.class == Date
							v = value.strftime("%Y-%m-%d")
							v = "\"#{v}\"" if json
						elsif value.class == Fixnum
							v = "#{value}"
						elsif value.class == Float
							v = "#{value}"
						elsif value.class == Integer
							v = "#{value}"
						elsif value.class == TrueClass
							v = "t"
							v = "\"#{v}\"" if json
						elsif value.class == FalseClass
							v = "f"
							v = "\"#{v}\"" if json
						else
							v = "#{value}"
							v = "\"#{v}\"" if json
						end
						v
					end
				end
			end
		end
	end
end
