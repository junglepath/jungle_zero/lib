require_relative '../io/db'
require_relative '../import'

module Jungle
	module Db
		module Access
			module Import
				class Schema
					def initialize(config, logger=nil)
						@logger = logger
						@path = config.import_file_path
					end

					def drop_table(table_class)
						file_name = Jungle::Db::Access::Import.data_file_name(@path, table_class.table_name)
						File.delete file_name if File.exist?(file_name)
					end

					def create_table(table_class)
						raise 'create_table method not implemented'
					end
				end
			end
		end
	end
end
