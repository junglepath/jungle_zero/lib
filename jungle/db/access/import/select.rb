require_relative '../../model/params'
require_relative '../io/db'
require_relative '../import'

module Jungle
	module Db
		module Access
			module Import
				class Select
					def initialize(config, logger=nil)
						@logger = logger
						@path = config.import_file_path
					end

					def _model(model)
						nil
					end

					def _model_by_any(model)
						nil
					end

					def _models(model)
						from_file(model)
					end

					def _models_from_file(model)
						from_file(model)
					end

					def from_file(model)
						# select all of a given model.
						file_name = Jungle::Db::Access::Import.data_file_name(@path, model.class.table_name)
						puts "file_name: #{file_name}."
						models = []
						lines = File.readlines(file_name)
						puts "lines.count: #{lines.count}."
						lines.each do |line|
							values = line.split("\t")
							hash = to_hash(model, values)
							params = Jungle::Db::Model::Params.transform(hash, model.class.columns)
							models << model.class.new(params, false)
						end
						models
					end

					def to_hash(model, values)
						hash = {}
						i = 0
						model._columns.keys.each do |key|
							value = values[i]
							if value == '\N'
								value = nil
							end
							hash[key] = value
							i += 1
						end
						hash
					end
				end
			end
		end
	end
end
