require 'sequel'
require 'jungle/db/access/io/config'
require 'jungle/db/access/io/chunked_file_reader'
require 'jungle/db/access/io/copy'
require 'jungle/db/access/io/db'
require 'jungle/db/access/io/delete'
require 'jungle/db/access/io/init'
require 'jungle/db/access/io/insert'
require 'jungle/db/access/io/schema'
require 'jungle/db/access/io/select'
require 'jungle/db/access/io/update'

module Jungle
	module Db
		module Access
			module Io
				Dbc = ::Jungle::Db::Access::Io::Config
				extend self
				def connection_from_config(config)
					config = Dbc.struct(config) if config.class == Hash
					connection(
						database_type: config.type,
						user_name: config.user_name,
						database_name: config.name,
						host: config.host,
						extensions: config.extensions,
						password: config.password,
						port: config.port,
						options: config.options
					)
				end
				def connection_from_config_unknown_database(config)
					config = Dbc.struct(config) if config.class == Hash
					puts "config.type: #{config.type}."
					if config.type == 'postgres'
						connection_from_config_use_postgres_db(config)
					elsif config.type == 'tinytds'
						connection_from_config_use_ms_sql_server_db(config)
					else
						throw "Unknown database type: #{config.type}."
					end
				end
				def connection_from_config_use_ms_sql_server_db(config)
					config = Dbc.struct(config) if config.class == Hash
					db = connection(
						database_type: config.type,
						user_name: config.user_name,
						database_name: 'master',
						host: config.host,
						extensions: config.extensions,
						password: config.password,
						port: config.port,
						options: config.options
					)
				end
				def connection_from_config_use_postgres_db(config)
					config = Dbc.struct(config) if config.class == Hash
					db = connection(
						database_type: config.type,
						user_name: config.user_name,
						database_name: 'postgres',
						host: config.host,
						extensions: config.extensions,
						password: config.password,
						port: config.port,
						options: config.options
					)
				end
				def clone_config(config)
					config = Dbc.struct(config) if config.class == Hash
					clone = Jungle::Db::Access::Io::Config.hash(
						name: config.name,
						type: config.type,
						user_name: config.user_name,
						password: config.password,
						host: config.host,
						extensions: config.extensions,
						port: config.port,
						options: config.options
					)
					Dbc.struct(clone)
				end
				def connection(database_type:, user_name:, database_name:, host: "localhost", extensions: [], password: nil, port: nil, options: nil)
					Sequel.default_timezone = :utc
					colon_port = ":#{port}" if port
					options = {} unless options
					if password
						puts "Using db connection with explicit password!"
						connection_string = "#{database_type}://#{user_name}:#{password}@#{host}#{colon_port}/#{database_name}"
						puts "connection_string: #{connection_string}."
						db = Sequel.connect(connection_string, options)
						puts "connected."
					else
						puts "no explicit pw."
						connection_string = "#{database_type}://#{user_name}@#{host}#{colon_port}/#{database_name}"
						puts "connection_string: #{connection_string}."
						db = Sequel.connect(connection_string, options)
						puts "connected."
					end
					extensions.each do |extension|
						puts "Adding Sequel extension: #{extension}."
						db.extension extension
					end
					db
				end
			end
		end
	end
end
