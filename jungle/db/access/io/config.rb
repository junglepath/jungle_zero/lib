module Jungle
	module Db
		module Access
			module Io
				module Config
					Db = Struct.new(:name, :type, :user_name, :password, :host, :extensions, :port, :options)
					def self.hash(name: nil, type: nil, user_name: nil, password: nil, host: nil, extensions: [], port: nil, options: nil)
						{
							name: name,
							type: type,
							user_name: user_name,
							password: password,
							host: host,
							extensions: extensions,
							port: port,
							options: options
						}
					end
					def self.struct(hash)
						s = hash if hash.class == Jungle::Db::Access::Io::Config::Db
						s = Db.new(*hash.values_at(*Db.members)) if hash.class == Hash
						s
					end
				end
			end
		end
	end
end
