require 'sequel'
require_relative './chunked_file_reader'
require_relative './init'

module Jungle
	module Db
		module Access
			module Io
				class Copy
					include Jungle::Db::Access::Io::Init

					def from_table model_class
						nil
					end

					def into_table model_class, data_file
						done = false
						loops = 0
						until done
							loops +=1
							puts "loops: #{loops}."
							error = nil
							file_reader = Jungle::Db::Access::Io::ChunkedFileReader.new data_file unless file_reader # must work with chunks of lines since we won't know which exact line was bad if there is a postgresql error.
							begin
								@db.copy_into(model_class.table_name, :data => file_reader)
							rescue Sequel::ForeignKeyConstraintViolation => e
								error = e
							rescue Sequel::UniqueConstraintViolation => e
								error = e
							rescue Sequel::DatabaseError => e
								error = e
							rescue Exception => e
								error = e
							end
							if error
								if file_reader.chunk_size == 1
									log_error "bad data: exception: #{error.class}."
									log_error "  message: #{error.message.gsub("\n", '\n').gsub("\r", '\r')}."
									log_error "  was_bad_chunk: start_at: #{file_reader.start_at}, chunk_size: #{file_reader.chunk_size}."
									line = file_reader.line
									log_error "  line: #{line.gsub("\n", '\n').gsub("\r", '\r')}."
									parts = line.split("\t")
									parts.each_with_index do |part, index|
										log_error "    #{index}: part: #{part}."
									end
								end
								file_reader.was_bad_chunk
							else
								puts "was_good_chunk: start_at: #{file_reader.start_at}, chunk_size: #{file_reader.chunk_size}."
								file_reader.was_good_chunk
							end
							done = file_reader.done
							if done
								log "done with #{model_class.table_name}."
							end
						end
					end

					def log_error(msg)
						#binding.pry
						puts "log error: #{msg}"
						@logger.error(msg)
					end

					def log(msg)
						#binding.pry
						puts "log info: #{msg}"
						@logger.info(msg) if @logger
					end
				end
			end
		end
	end
end
