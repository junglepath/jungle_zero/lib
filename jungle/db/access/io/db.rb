require 'jungle/db/access/meta/table'
require 'jungle/db/access/io'
require 'jungle/db/access/io/config'
require 'jungle/db/access/io/select'
require 'jungle/db/access/io/insert'
require 'jungle/db/access/io/update'
require 'jungle/db/access/io/delete'
require 'jungle/db/access/io/schema'
require 'jungle/db/access/io/copy'

module Jungle
	module Db
		module Access
			module Io
				class Db
					attr_reader :select
					attr_reader :insert
					attr_reader :update
					attr_reader :delete
					attr_reader :schema
					attr_reader :copy
					attr_reader :base
					attr_reader :database_name
					attr_reader :config
					attr_reader :postgresql

					def initialize(config, logger=nil)
						config = Jungle::Db::Access::Io::Config.struct(config)
						@logger = logger
						@config = config
						@postgresql = config
						@db = Jungle::Db::Access::Io.connection(
							database_type: config.type,
							user_name: config.user_name,
							database_name: config.name,
							host: config.host,
							extensions: config.extensions,
							password: config.password,
							port: config.port,
							options: config.options
						)
						@select = Jungle::Db::Access::Io::Select.new @db, @logger
						@insert = Jungle::Db::Access::Io::Insert.new @db, @logger
						@update = Jungle::Db::Access::Io::Update.new @db, @logger
						@delete = Jungle::Db::Access::Io::Delete.new @db, @logger
						@schema = Jungle::Db::Access::Io::Schema.new @db, @logger
						@copy = Jungle::Db::Access::Io::Copy.new @db, @logger
						@base = @db
						@database_name = config.name
					end

					def drop_table? table_name
						Jungle::Db::Access::Meta::Table.drop? self, table_name
					end

					def table_exists? table_name
						Jungle::Db::Access::Meta::Table.exists? self, table_name
					end

					def rename_table table_name, new_table_name
						Jungle::Db::Access::Meta::Table.rename_table self, table_name, new_table_name
					end

					def create_table_like(from_table, to_table)
						Jungle::Db::Access::Meta::Table.create_like(self, from_table, to_table)
					end

					def copy_table_data(from_table, to_table)
						Jungle::Db::Access::Meta::Table.copy_data(self, from_table, to_table)
					end

					def reset_sequence_for_table(table_name)
						#max = @db[table_name.to_sym].max(:id) + 1
						#@db.run "alter sequence #{table_name}_id_seq restart with #{max}"
						@db.run "select setval('#{table_name}_id_seq', (select max(id)+1 from \"#{table_name}\"), false)"
					end

					def get_max_id_for_table(table_name, id_column_name=:id)
						ds = @db["select max(#{id_column_name}) as max_id from \"#{table_name}\""]
						result = ds.first
						max_id = result[:max_id]
					end

					def transaction
						@db.transaction do
							yield
						end
					end

					private

					def begin_transaction
						# not implemented :)
					end

					def commit_transaction
						# not implemented :)
					end

					def rollback_transaction
						# not implemented :)
					end
				end
			end
		end
	end
end
