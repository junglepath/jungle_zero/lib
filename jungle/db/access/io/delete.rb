module Jungle
	module Db
		module Access
			module Io
				require 'jungle/db/access/io/init'

				class Delete
					include Jungle::Db::Access::Io::Init

					def _model(model)
						ds = @db[model._meta.table_name].where(model._primary_key)
						#puts ds.sql
						ds.delete
					end

					def _models(model, confirm=false)
						ds = @db[model._meta.table_name].where(handle_json_columns(model, model._has_value_hash))
						return ds.delete if confirm
						return ds.sql
					end
				end
			end
		end
	end
end
