require 'sequel'

module Jungle
	module Db
		module Access
			module Io
				module Init
					def initialize(db, logger=nil)
						@db = db
						@logger = logger
					end

					def handle_json_columns(model, hash)
						new_hash = {}
						columns = model._meta.columns
						hash.each do |key, value|
							puts "key: #{key}"
							if columns[key].type == :json
								unless value == nil
									#if value.class == String # must convert to hash... (assuming this is a string of json...)
									#	value = JsonWrap.load value, {:mode => :compat, :symbol_keys => true, :indent=>0 }
									#end
									new_hash[key] = Sequel.pg_json(value)
								end
							elsif columns[key].type == :jsonb
								unless value == nil
									#if value.class == String # must convert to hash... (assuming this is a string of json...)
									#	value = JsonWrap.load value, {:mode => :compat, :symbol_keys => true, :indent=>0 }
									#end
									new_hash[key] = Sequel.pg_jsonb(value)
								end
							else
								new_hash[key] = value unless value == nil
							end
						end
						new_hash
					end
				end
			end
		end
	end
end
