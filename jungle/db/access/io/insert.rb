require 'jungle/db/access/io/init'
require 'jungle/db/model/table'

module Jungle
	module Db
		module Access
			module Io
				class Insert
					include Jungle::Db::Access::Io::Init

					def _model(model)
						insert = handle_json_columns(model, model.to_h)
						puts "inserting: #{insert}."
						#@db[model._table_name] << insert
						result = @db[model._meta.table_name].insert(insert)
						if model._meta.primary_key_columns.count == 1
							#model._values[model._primary_key_columns.keys.first] = result
							model[model._meta.primary_key_columns.keys.first] = result
						end
						#model._secure = false #allow 'secure' columns to be included in hash.
						#model = model.class.new(model.to_hash, false) # create a new model instance with all values marked as unmodified.
						#model = model.class.new(model.to_h, model._meta)
						model = Jungle::Db::Model::Table::Data.new(model.to_h, model._meta, false, false)
						model
					end
				end
			end
		end
	end
end
