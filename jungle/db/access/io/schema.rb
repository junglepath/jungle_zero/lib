require 'jungle/db/access/meta'
require 'jungle/db/access/io/init'

module Jungle
	module Db
		module Access
			module Io
				class Schema
					include Jungle::Db::Access::Io::Init

					def drop_table(table_meta)
						Jungle::Db::Access::Meta::Schema.drop_table(@db, table_meta)
					end

					def create_table(table_meta)
						Jungle::Db::Access::Meta::Schema.create_table(@db, table_meta)
					end

					def reset_table(table_meta)
						drop_table(table_meta)
						create_table(table_meta)
					end
				end
			end
		end
	end
end
