require 'jungle/db/access/io/init'
require 'jungle/db/model/table'

module Jungle
	module Db
		module Access
			module Io
				class Select
					include Jungle::Db::Access::Io::Init

					def _model(model)
						# select based on a model's primary key.
						ds = @db[model._meta.table_name].where(model._primary_key)
						#puts ds.sql
						hash = ds.first
						return nil unless hash
						new_model = to_model(model._meta, hash, false) # false since row (initial values) from db is considered unmodified.
						#new_model = model.class.new(hash, model._meta) # false since row (initial values) from db is considered unmodified.
					end

					def _model_by_any(model)
						ds = @db[model._table_name].where(handle_json_columns(model, model._modified_hash))
						#puts ds.sql
						hash = ds.first
						#puts "hash: #{hash}. Nil? #{hash == nil}."
						return nil unless hash
						new_model = to_model(model._meta, hash, false) # false since row (initial values) from db is considered unmodified.
						#new_model = model.class.new(hash, false) # false since row (initial values) from db is considered unmodified.
					end

					def _models(model, *order_by)
						# select all of a given model.
						ds = @db[model._meta.table_name]
						ds = ds.where(handle_json_columns(model, model._has_value_hash)) if model._has_value_hash.length > 0
						ds = ds.order(*order_by) if order_by.length > 0
						ds = ds.order(model._meta.primary_key_columns.keys) unless order_by.length > 0
						#puts ds.sql
						rows = ds.all
						models = []
						rows.each do |row|
							models << to_model(model._meta, row, false) # false since row (initial values) from db is considered unmodified.
							#models << model.class.new(row, false)
						end
						#puts "models:"
						#puts models.inspect
						models
					end

					private

					def to_model(meta, hash, init_nil=false, transform_params=false)
						Jungle::Db::Model::Table::Data.new(hash, meta, transform_params, init_nil)
					end
				end
			end
		end
	end
end
