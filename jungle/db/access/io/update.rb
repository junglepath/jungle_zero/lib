module Jungle
	module Db
		module Access
			module Io
				require 'jungle/db/access/io/init'

				class Update
					include Jungle::Db::Access::Io::Init

					def _model(model)
						update_hash = handle_json_columns(model, model._modified_hash)
						count = update_by_primary_key model, update_hash
						return {update_count: count}
					end

					def _models(model_filter, model_values, confirm=false)
						ds = @db[model_filter._meta.table_name].where(handle_json_columns(model_filter, model_filter._has_value_hash))
						#puts ds.sql
						#puts "set values: #{model_values._has_value_hash}."
						count = 0
						count = ds.update(handle_json_columns(model_values, model_values._has_value_hash)) if confirm
						return {update_count: count}
					end

					private

					def update_by_primary_key model, update_hash
						@db.transaction do
							ds = @db[model._meta.table_name].where(model._primary_key).for_update
							#puts ds.sql
							ds.update update_hash
						end
					end
				end
			end
		end
	end
end
