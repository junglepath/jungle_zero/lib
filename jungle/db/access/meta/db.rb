# gen_db.rb -- drops and creates a postgresql database from the commandline dropdb/createdb commands.
require 'jungle/db/access/io'
require 'jungle/db/access/io/config'

module Jungle
  module Db
    module Access
      module Meta
        module Db
          Dbc = Jungle::Db::Access::Io::Config
          
          def self.create(config)
            config = Dbc.struct(config) if config.class == Hash
            puts "Jungle::Db::Access::Meta::Db.create: #{config.name}."
            db = Jungle::Db::Access::Io.connection_from_config_use_postgres_db(config)
            sql = "create database #{config.name}"
            db.run sql
          end

          def self.drop!(config)
            config = Dbc.struct(config) if config.class == Hash
            puts "Jungle::Db::Access::Meta::Db.drop: #{config.name}."
            kill_connections! config
            db = Jungle::Db::Access::Io.connection_from_config_use_postgres_db(config)
            sql = "drop database #{config.name}"
            db.run sql
          end

          def self.drop?(config)
            config = Dbc.struct(config) if config.class == Hash
            if exists?(config)
              drop! config
            end
          end

          def self.reset!(config)
            config = Dbc.struct(config) if config.class == Hash
            drop? config
            create config
          end

          def self.exists?(config) # todo: fix to also work for ms sql server.
            config = Dbc.struct(config) if config.class == Hash
            puts "Jungle::Db::Access::Meta::Db.exists? #{config.name}."
            exists = false
            db = Jungle::Db::Access::Io.connection_from_config_unknown_database(config)
            sql = sql_query_db_existence(config)
            db.fetch(sql) do |row|
              exists = true
            end
            exists
          end

          def self.rename(config_from, to_name)
            config_from = Dbc.struct(config_from) if config_from.class == Hash
            puts "Jungle::Db::Access::Meta::Db.rename: #{config_from.name} to #{to_name}."
            kill_connections! config_from
            db = Jungle::Db::Access::Io.connection_from_config_use_postgres_db(config_from)
            sql = "alter database #{config_from.name} rename to #{to_name}"
            db.run sql
          end

          def self.rename?(config_from, to_name)
            config_from = Dbc.struct(config_from) if config_from.class == Hash
            if exists?(config_from)
              rename config_from, to_name
            end
          end

          def self.kill_connections!(config)
            config = Dbc.struct(config) if config.class == Hash
            db = Jungle::Db::Access::Io.connection_from_config_use_postgres_db(config)
            sql = "select pg_terminate_backend(pid) from pg_stat_activity where datname = '#{config.name}'"
            db.run sql
          end

          private

          def self.sql_query_db_existence(config)
            config = Dbc.struct(config) if config.class == Hash
            if config.type == 'postgres'
              "select datname from pg_database where datname = '#{config.name}'"
            elsif config.type == 'tinytds'
              "select * from master.sys.databases where name = '#{config.name}'"
            else
              throw "Unknown database type: #{config.type}."
            end
          end
        end
      end
    end
  end
end
