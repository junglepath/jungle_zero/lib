# Creates a database schema from a set of table structures.
require 'logger'
require 'sequel'
require 'jungle/db/model'
require 'jungle/db/access'
require 'jungle/db/access/io/config'

module Jungle
	module Db
		module Access
			module Meta
				module Schema
					Dbc = Jungle::Db::Access::Io::Config

					def self.create(table_metas, db_config, logger=$stdout)
						db_config = Dbc.struct(db_config)
						puts "Jungle::Db::Access::Meta::Schema.create..."
						db = Jungle::Db::Access::Io.connection_from_config(db_config)
						db.loggers << Logger.new(logger)
						table_metas.each do |table|
							if table.is_view?
								puts "create view: #{table.table_name}"
								db.run(table.view.create)
							else
								puts "create table: #{table.table_name}"
								create_table db, table
							end
						end
					end

					def self.create_table db, table_meta
						# translate column type keys into Sequel methods used by Sequel (DB.create_table).
						# call the Sequel DB.create_table method with a block:
						#puts "\ttable_name: #{table_meta.table_name}"
						schema = self
						db.create_table? table_meta.table_name do
							made_pk = false
							fk_alias = {}
							table_meta.columns.each_value do |column|
								puts "\tcolumn: #{column.name} #{schema.get_sequel_method(column.type)} #{column.foreign_key_table_name}."
								method = schema.get_sequel_method(column.type, (table_meta.primary_key_columns.count > 1))
								args_hash = {}
								if column.foreign_key? and column.not_null?
									#args = [column.name, column.foreign_key_table_name]
									args = [column.name]
									args_hash[:null] = false
								elsif column.foreign_key?
									#args = [column.name, column.foreign_key_table_name]
									args = [column.name]
								elsif column.primary_key?
									args = [column.name]
									#made_pk = true
								else
									args = [column.name]
								end
								args_hash[:unique] = true if column.unique?
								args_hash[:null] = false if column.not_null?
								args_hash[:type] = schema.get_postgresql_override_type(column.override_type) unless column.override_type == nil
								args_hash[:default] = column.default_value unless column.default_value == nil
								args << args_hash
								puts "\t\tmethod: #{method}. args:#{args}.\n"
								made_pk = true if column.type == :primary_key and table_meta.primary_key_columns.count == 1 # should only happen when pk is single column.
								puts "column: #{column.name}, override_type: #{column.override_type}, override_type.class: #{column.override_type.class}, method: #{method}, args: #{args}."
								send(method, *args)

								#index:
								if column.index
									send('index', column.index)
								end

								#unique index:
								if column.unique_index
									send('unique', column.unique_index)
								end
							end

							if not made_pk
								#puts "creating primary_key for table #{table_meta.name}. keys: #{table_meta.primary_key_columns.keys}."
								send('primary_key', table_meta.primary_key_columns.keys, :name=>"#{table_meta.table_name}_pk".to_sym)
							end

							table_meta.foreign_key_columns_by_table_alias.each do |k, h|
								fk_columns = h.values.map{|c| c.name}
								fk_table = h.values[0].foreign_key_table_name
								send('foreign_key', fk_columns, fk_table, :name=>"#{fk_table}_#{fk_columns.join('_')}_fk".to_sym)
							end

							#puts "end of block"
						end
					end

					def self.drop_table db, table_meta
						db.drop_table? table_meta.table_name
					end

					def self.get_postgresql_override_type type
						return 'text' if type == :string
						return 'integer' if type == :integer
						return 'bigint' if type == :bigint
						return 'timestamp' if type == :timestamp
						return 'timestamp' if type == :timestamp_local
						return 'boolean' if type == :boolean
						return 'date' if type == :date
						return 'json' if type == :json
						return 'jsonb' if type == :jsonb
						return 'double' if type == :float
						return type.to_s
					end

					def self.get_sequel_method type, multi_column_k=false
						#return 'foreign_key' if type == :foreign_key
						return 'Integer' if type == :foreign_key
						return 'primary_key' if type == :primary_key and multi_column_k == false
						return 'Integer' if type == :primary_key and multi_column_k == true
						return 'String' if type == :string
						return 'Integer' if type == :integer
						return 'DateTime' if type == :timestamp
						return 'DateTime' if type == :timestamp_local
						return 'TrueClass' if type == :boolean
						return 'Date' if type == :date
						return 'json' if type == :json
						return 'jsonb' if type == :jsonb
						return 'Float' if type == :float
						return type
					end

					def self.set_version schema_info_class, db_config, schema_initial_version
						db_config = Dbc.struct(db_config)
						# set starting version numnber
						db = Jungle::Db::Access::Io::Db.new(db_config)
						db.schema.create_table schema_info_class
						if schema_initial_version
							schema_info = schema_info_class.new({version: schema_initial_version})
							db.insert._model(schema_info)
						end
					end

					def self.set_version_to_latest schema_info_class, db_config, migrations_path
						db_config = Dbc.struct(db_config)
						version = 0
						migration_files = Dir.glob(File.join(migrations_path, "*.rb"))
						migration_files.each do |file_name|
							puts file_name
							parts = File.basename(file_name).split('_')
							if parts.length > 0
								value = parts[0].to_i
								version = value if value > version
							end
						end
						set_version schema_info_class, db_config, version
					end
				end
			end
		end
	end
end
