# table manipulation.
# db.base is a Sequel database object.
# db is can be a Jungle::Db::Access::IO::DB object.
module Jungle
  module Db
    module Access
      module Meta
        module Table
          def self.drop? db, table_name
            db.base.drop_table?(table_name.to_sym)
          end

          def self.exists? db, table_name
            db.base.table_exists?(table_name.to_sym)
          end

          def self.rename_table db, table_name, new_table_name
            db.base.rename_table(table_name, new_table_name)
          end

          def self.create_like(db, from_table, to_table)
            db.base.run("create table #{to_table} (like #{from_table} including indexes)")
          end

          def self.copy_data(db, from_table, to_table)
            db.base.run("insert into #{to_table} select * from #{from_table}")
          end
        end
      end
    end
  end
end
