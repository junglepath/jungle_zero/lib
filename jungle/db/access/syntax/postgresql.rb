module Jungle
	module Db
		module Access
			module Syntax
				module Postgresql
					def self.sql_true
						"true"
					end
					def self.sql_allow_order_bys_in_sub_select
						""
					end
					def self.left_bracket
						""
					end
					def self.right_bracket
						""
					end
					def self.convert_nvarchar_beg
						""
					end
					def self.convert_nvarchar_end
						"::text"
					end
				end
			end
		end
	end
end
