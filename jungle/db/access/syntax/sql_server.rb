module Jungle
	module Db
		module Access
			module Syntax
				module SqlServer
					def self.sql_true
						"1"
					end
					def self.sql_allow_order_bys_in_sub_select
						"top 1000000000"
					end
					def self.left_bracket
						"["
					end
					def self.right_bracket
						"]"
					end
					def self.convert_nvarchar_beg
						"convert(nvarchar, "
					end
					def self.convert_nvarchar_end
						")"
					end
				end
			end
		end
	end
end
