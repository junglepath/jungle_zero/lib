module Jungle
  module Db
    module Model
      module Column
        def self.transform_to_meta_column_hash(array, sequence)
          name = array[0]
          type = array[1]
          primary_key = array.include? :primary_key
          foreign_key = array.include? :foreign_key
          foreign_key_table_name = array[array.index(:foreign_key) + 1] if foreign_key && array[array.index(:foreign_key) + 1].class == Symbol
          foreign_key_table_name = array[array.index(:foreign_key) + 1][0] if foreign_key && array[array.index(:foreign_key) + 1].class == Array
          foreign_key_table_alias = array[array.index(:foreign_key) + 1][1] if foreign_key && array[array.index(:foreign_key) + 1].class == Array
          not_null = ((array.include? :not_null) or primary_key)
          unique = array.include? :unique
          contains_unique_index = array.include? :unique_index
          unique_index = array[array.index(:unique_index) + 1] if contains_unique_index
          contains_index = array.include? :index
          index = array[array.index(:index) + 1] if contains_index
          secure = array.include? :secure
          alternate_key = array.include? :alternate_key
          calculated = array.include? :calculated
          description = array[array.index(:desc) + 1] if array.include? :desc
					override_type = nil
          override_type = array[array.index(:override_type) + 1] if array.include? :override_type
          default_index = array.index(:default)
          default_value = array[default_index + 1] if default_index and default_index > 0
          tags = array[array.index(:tags) + 1] if array.include? :tags
          base_type = get_base_type(type, override_type)
          ruby_type = get_ruby_type(base_type, type)
          hash = {
            name: name,
            type: type,
            base_type: base_type,
            foreign_key_table_name: foreign_key_table_name,
            foreign_key_table_alias: foreign_key_table_alias,
            sequence: sequence,
            tags: tags,
            override_type: override_type,
            description: description,
            index: index,
            unique_index: unique_index,
            default_value: default_value,
            primary_key: primary_key,
            alternate_key: alternate_key,
            foreign_key: foreign_key,
            not_null: not_null,
            calculated: calculated,
            unique: unique,
            secure: secure,
            ruby_type: ruby_type
          }
          hash
        end
        def self.get_base_type(type, override_type)
          base_type = type
          if type == :primary_key or type == :foreign_key
            base_type = :integer
          end
          base_type = override_type if override_type
          base_type
        end
        def self.get_ruby_type(base_type, type)
          return String if base_type == :string
          return Integer if base_type == :integer
          return DateTime if base_type == :timestamp
          return DateTime if base_type == :timestamp_local
          return TrueClass if base_type == :boolean
          return Date if base_type == :date
          return String if base_type == :json
          return String if base_type == :jsonb
          return Float if base_type == :float
          return Integer if type == :foreign_key
          return Integer if type == :primary_key
          nil
        end
        class Meta
          def initialize(meta_column_hash)
            @hash = meta_column_hash
          end
          def name() @hash[:name] end
          def type() @hash[:type] end
          def base_type() @hash[:base_type] end
          def foreign_key_table_name() @hash[:foreign_key_table_name] end
          def foreign_key_table_alias() @hash[:foreign_key_table_alias] end
          def sequence() @hash[:sequence] end
          def tags() @hash[:tags] end
          def override_type() @hash[:override_type] end
          def description() @hash[:description] end
          def index() @hash[:index] end
          def unique_index() @hash[:unique_index] end
          def default_value() @hash[:default_value] end
          def primary_key?() @hash[:primary_key] end
          def alternate_key?() @hash[:alternate_key] end
          def foreign_key?() @hash[:foreign_key] end
          def not_null?() @hash[:not_null] end
          def calculated?() @hash[:calculated] end
          def unique?() @hash[:unique] end
          def secure?() @hash[:secure] end
          def ruby_type() @hash[:ruby_type] end
          def [](key) @hash[key] end
          def to_hash() @hash end
          def to_h() @hash end
        end
      end
    end
  end
end
