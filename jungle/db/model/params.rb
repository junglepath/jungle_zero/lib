module Jungle
	module Db
		module Model
			module Params
				extend self
				def transform(params, columns)
					# sinatra params come as string keys with string values.
					# Convert values to various types based on Schema column definitions.
					# Convert keys to symbols.
					p = params.to_hash
					p = symbolize(p)
					p = convert_to_type(p, columns)
					p
				end

				def symbolize(params)
					p = params.to_hash
					Hash[p.map {|k, v| [k.to_sym, v] }]
				end

				def convert_to_type(params, columns)
					#params must be hash of symbols (as keys) and values
					p = {}
					params = params.to_hash
					params.each do |k, v|
						column = columns[k]
						if column and v
							#puts "column, type: #{k} #{column.type}."
							if column.type == :integer or column.base_type == :integer
								p[k] = v.to_i
							elsif column.type == :timestamp and v.class == String
								p[k] = Time.parse(v).utc
							elsif column.type == :timestamp_local and v.class == String # experimental, probably won't use...
								p[k] = Time.parse(v)
							elsif column.type == :date and v.class == String
								p[k] = Date.parse(v).to_time
							elsif column.type == :date and v.class == Date
								p[k] = v.to_time
							elsif column.type == :boolean
								p[k] = to_bool(v)
							elsif (column.type == :string or column.base_type == :string) and v.class == String and v.strip.empty?
								# treat empty/whitespace only strings as nil.
								p[k] = nil
							else
								p[k] = v
							end
						else
							p[k] = v
						end
					end
					p
				end

				def to_bool(value)
					return false if value == nil
					return true  if value == true   || value =~ (/(true|t|yes|y|on|1)$/i)
					return false if value == false  || value.empty? || value =~ (/(false|f|no|n|off|0)$/i)
					raise ArgumentError.new("invalid value for Boolean: \"#{value}\"")
				end
			end
		end
	end
end
