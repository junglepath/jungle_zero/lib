module Jungle
	module Db
		module Model
			module Standard
				module NamedColumnSets
					extend self
					def audit_user
						{name: :audit_user,
							columns: [
								[:created_by_user_id, :foreign_key, :user],
								[:created_at, :timestamp, :default, 'now()'],
								[:updated_by_user_id, :foreign_key, :user],
								[:updated_at, :timestamp, :default, 'now()']
							]
						}
					end
					def zaudit_user
						{name: :audit_user,
							columns: [
								[:created_by, :foreign_key, :user],
								[:created_at, :timestamp, :default, :fn_now],
								[:updated_by, :foreign_key, :user],
								[:updated_at, :timestamp, :default, :fn_now]
							]
						}
					end
				end
			end
		end
	end
end
