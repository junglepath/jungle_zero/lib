module Jungle
	module Db
		module Model
			module Standard
				module Tables
					def self.role
						{name: :role,
							description: "a named security principal -- set of permissions and restrictions",
							columns: [
								[:id, :primary_key],
								[:role, :string],
								[:named_column_set, :audit_user]
							],
							view: nil
						}
					end
					def self.schema_info
						{name: :schema_info,
							description: "Table used by the Sequel Ruby gem to track database migration versions.",
							columns: [
								[:version, :integer, :primary_key]
							],
							view: nil
						}
					end
					def self.user
						{name: :user,
							description: "authenticated user of the system",
							columns: [
								[:id, :primary_key],
								[:user_name, :string, :unique, :not_null, :alternate_key],
								[:password_hash, :string, :not_null, :secure, :calculated, :desc, "See password_hash.rb for details."],
								[:name, :string, :desc, "Your human name! :)"],
								[:email, :string],
								[:mobile, :string],
								[:organization, :string, :secure],
								[:notes, :string, :desc, "For any misc. notes about this user including addition email addresses, phone numbers, etc."],
								[:active, :boolean, :default, true],
								[:is_valid, :boolean, :calculated],
								[:role, :string, :not_null],
								[:sms_verification_code, :string, :secure, :desc, "phone verification/sign-in code."],
								[:activation_key, :string, :secure],
								[:password_reset_code, :string, :secure],
								[:named_column_set, :audit_user]
							],
							plural: "users"
						}
					end
					def self.user_role
						{name: :user_role,
							description: "associates a user with a role.",
							columns: [
								[:user_id, :foreign_key, :user, :primary_key],
								[:role_id, :foreign_key, :role, :primary_key],
								[:named_column_set, :audit_user]
							]
						}
					end
				end
			end
		end
	end
end
