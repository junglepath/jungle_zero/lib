class ::String
  def underscore
    self.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end
	def proper_case
		self.split('_').map do |s|
			if s.length > 0
				s[0].upcase + s[1..-1]
			else
				''
			end
		end.join()
	end
end
