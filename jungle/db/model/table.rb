require 'set'
require 'jungle/json'
require 'jungle/db/model'
require 'jungle/db/model/column'
require 'jungle/db/model/view'
module Jungle
	module Db
		module Model
			module Table
				def self.to_ers(models_hash)
					ers = Struct.new(:models, :ulinks, :dlinks, keyword_init: true)
					ulinks = up_links(models_hash)
					dlinks = down_links(models_hash)
					ers.new(models: models_hash, ulinks: ulinks, dlinks: dlinks)
				end
				def self.to_metas_array(schema_array, named_column_sets={})
					schema_array.map do |h|
						transform_to_meta_table_hash(h, named_column_sets)
					end
				end
				def self.to_models_array(schema_array, named_column_sets={})
					to_metas_array(schema_array, named_column_sets).map do |m|
						Meta.new(m)
					end
				end
				def self.to_models_hash(models_array)
					hash = {}
					models_array.each do |model|
						hash[model.name] = model
					end
					hash
				end
				def self.up_links(models_hash)
					ulinks = {}
					models_hash.each do |k, model|
						model.foreign_key_columns_by_table_alias.each do |aka, fk|
							fk_table_name = fk.values[0].foreign_key_table_name
							ulinks[fk_table_name] = ulinks[fk_table_name] || {}
							ulinks[fk_table_name]["#{model.name}:#{aka}"] = model.name
						end
					end
					ulinks
				end
				def self.down_links(models_hash)
					dlinks = {}
					models_hash.each do |k, model|
						dlinks[k] = dlinks[k] || {}
						model.foreign_key_columns_by_table_alias.each do |aka, fk|
							dlinks[k][aka] = fk.values[0].foreign_key_table_name
						end
					end
					dlinks
				end
				def self.transform_to_meta_table_hash(hash, named_column_sets={})
					sequence = -1
					columns = merge_named_column_sets(hash[:columns], named_column_sets)
					columns = columns.map do |column|
						sequence += 1
						meta_hash = Jungle::Db::Model::Column.transform_to_meta_column_hash(column, sequence)
					end
					columns_h = {}
					primary_key_columns_h = {}
					alternate_key_columns_h = {}
					foreign_key_columns_h = {}
					foreign_key_columns_by_table_name_h = {}
					foreign_key_columns_by_table_alias_h = {}
					references = Set.new
					secure_columns_h = {}
					unique_indexes = []
					indexes = []
					tagged_columns_h = {}
					columns.each do |column|
						columns_h[column[:name]] = column
						primary_key_columns_h[column[:name]] = column if column[:primary_key]
						alternate_key_columns_h[column[:name]] = column if column[:alternate_key]
						foreign_key_columns_h[column[:name]] = column if column[:foreign_key]
						if column[:foreign_key]
							sub_hash = foreign_key_columns_by_table_name_h[column[:foreign_key_table_name]]
							sub_hash = {} unless sub_hash
							sub_hash[column[:name]] = column
							foreign_key_columns_by_table_name_h[column[:foreign_key_table_name]] = sub_hash

							t_alias = column[:foreign_key_table_alias] || column[:foreign_key_table_name]
							alias_sub_hash = foreign_key_columns_by_table_alias_h[t_alias]
							alias_sub_hash = {} unless alias_sub_hash
							alias_sub_hash[column[:name]] = column
							foreign_key_columns_by_table_alias_h[t_alias] = alias_sub_hash
						end
						if column[:tags]
							column[:tags].each do |tag|
								tagged_columns = tagged_columns_h[tag] || Set.new
								tagged_columns.add(column)
								tagged_columns_h[tag] = tagged_columns
							end
						end
						references << column[:foreign_key_table_name] if column[:foreign_key] and column[:foreign_key_table_name] != hash[:name]
						secure_columns_h[column[:name]] = column if column[:secure]
					end
					columns_alphabetical_order = columns_h.values.clone.sort { |a,b| a[:name] <=> b[:name] }
					columns_sequence_order = columns_h.values.clone.sort { |a,b| a[:sequence] <=> b[:sequence] }
					hash[:view] = Jungle::Db::Model::View.transform_to_meta_view_hash(hash[:view])
					view_references = (hash[:view] && hash[:view][:references]) || references
					h = {
						name: hash[:name],
						table_name: hash[:name],
						name_proper_case: hash[:name].to_s.proper_case,
						plural_table_name: hash[:plural] || "#{hash[:name]}s",
						view: hash[:view],
						columns: columns_h,
						primary_key_columns: primary_key_columns_h,
						alternate_key_columns: alternate_key_columns_h,
						foreign_key_columns: foreign_key_columns_h,
						foreign_key_columns_by_table_name: foreign_key_columns_by_table_name_h,
						foreign_key_columns_by_table_alias: foreign_key_columns_by_table_alias_h,
						references: references,
						secure_columns: secure_columns_h,
						unique_indexes: unique_indexes,
						indexes: indexes,
						tagged_columns: tagged_columns_h,
						columns_alphabetical_order: columns_alphabetical_order,
						columns_sequence_order: columns_sequence_order,
						view_references: view_references
					}
				end
				def self.merge_named_column_sets(columns, named_column_sets)
					merged_columns = []
					columns.each do |column|
						if column[0] == :named_column_set and named_column_sets[column[1]]
							named_column_sets[column[1]][:columns].each do |c|
								merged_columns << c
							end
						elsif column.length == 1 && named_column_sets[column[0]]
							named_column_sets[column[0]][:columns].each do |c|
								merged_columns << c
							end
						else
							merged_columns << column
						end
					end
					merged_columns
				end
				def self.models_alphabetical_order(models_hash)
					models_hash.values.sort { |a,b| a.table_name <=> b.table_name }
				end
				def self.models_dependent_order(models_hash)
					result = self.model_dependent_sort(models_hash.values)
					# map array of table names into an array of table (model) classes.
					result.map {|key| models_hash[key]}
				end
				def self.model_dependent_sort(list, result=Set.new, level=0)
					# return array of table names sorted in dependency order (not alphabetical order).
					#puts "level: #{level}."
					remaining = []
					list.each do |value|
						#puts "\tvalue: #{value.table_name}. references: #{value.references.to_a}."
						if value.has_no_view_references?
							#puts "\tadding: #{value.table_name}."
							result << value.table_name
						elsif value.view_references.subset? result
							#puts "\tadding: #{value.table_name}."
							result << value.table_name
						else
							remaining << value
						end
					end
					#puts "\tresult: #{result.length}: #{result.to_a}."
					#puts "\tremaining: #{remaining.length}: #{remaining}."
					puts "level exceeded 10 -- something is wrong with your schema definition!!!" if level > 10
					return [] if level > 10 #something is wrong!!!
					result = self.model_dependent_sort(remaining, result, level + 1) if remaining.length > 0
					result
				end

				class Meta
					def initialize(meta_table_hash)
						@hash = meta_table_hash
						@hash[:view] = Jungle::Db::Model::View.view_meta_hash_to_meta_object(@hash[:view])
						@hash[:columns] = columns_meta_hash_hash_to_meta_object_hash(@hash[:columns])
						@hash[:primary_key_columns] = columns_meta_hash_hash_to_meta_object_hash(@hash[:primary_key_columns], @hash[:columns])
						@hash[:alternate_key_columns] = columns_meta_hash_hash_to_meta_object_hash(@hash[:alternate_key_columns], @hash[:columns])
						@hash[:foreign_key_columns] = columns_meta_hash_hash_to_meta_object_hash(@hash[:foreign_key_columns], @hash[:columns])

						new_hash = {}
						@hash[:foreign_key_columns_by_table_name].keys.each do |key|
							new_hash[key] = columns_meta_hash_hash_to_meta_object_hash(@hash[:foreign_key_columns_by_table_name][key], @hash[:columns])
						end
						@hash[:foreign_key_columns_by_table_name] = new_hash

						new_hash = {}
						@hash[:foreign_key_columns_by_table_alias].keys.each do |key|
							new_hash[key] = columns_meta_hash_hash_to_meta_object_hash(@hash[:foreign_key_columns_by_table_alias][key], @hash[:columns])
						end
						@hash[:foreign_key_columns_by_table_alias] = new_hash

						@hash[:columns_alphabetical_order] = columns_meta_hash_array_to_meta_object_array(@hash[:columns_alphabetical_order], @hash[:columns])
						@hash[:columns_sequence_order] = columns_meta_hash_array_to_meta_object_array(@hash[:columns_sequence_order], @hash[:columns])

						new_tagged_columns_hash = {}
						@hash[:tagged_columns].keys.each do |tag|
							meta_hash_set = @hash[:tagged_columns][tag]
							new_tagged_columns_hash[tag] = columns_meta_hash_set_to_meta_object_set(meta_hash_set, @hash[:columns])
						end
						@hash[:tagged_columns] = new_tagged_columns_hash

					end
					def name() @hash[:name] end
					def table_name() @hash[:table_name] end
					def plural_table_name() @hash[:plural_table_name] end
					def description() @hash[:description] end
					def columns() @hash[:columns] end
					def columns_alphabetical_order() @hash[:columns_alphabetical_order] end
					def columns_sequence_order() @hash[:columns_sequence_order] end
					def primary_key_columns() @hash[:primary_key_columns] end
					def alternate_key_columns() @hash[:alternate_key_columns] end
					def foreign_key_columns() @hash[:foreign_key_columns] end
					def foreign_key_columns_by_table_name() @hash[:foreign_key_columns_by_table_name] end
					def foreign_key_columns_by_table_alias() @hash[:foreign_key_columns_by_table_alias] end
					def tagged_columns() @hash[:tagged_columns] end
					def references() @hash[:references] end
					def secure_columns() @hash[:secure_columns] end
					def view() @hash[:view] end
					def view_references() @hash[:view_references] end
					def has_reference_to?(table_name) references.member?(table_name) end
					def has_references?() !references.empty? end
					def has_no_references?() !has_references? end
					def is_view?() (view && true) || false end
					def has_view_reference_to(table_name)
						return view.has_reference_to?(table_name) if view
						has_reference_to?(table_name)
					end
					def has_view_references?()
						return !view.references.empty? if view
						has_references?
					end
					def has_no_view_references?() !has_view_references? end
					def [](key) @hash[key] end
					def to_h() @hash end
					def to_hash() @hash end
					private
					def column_meta_hash_to_meta_object(column_meta_hash)
						Jungle::Db::Model::Column::Meta.new(column_meta_hash)
					end
					def columns_meta_hash_hash_to_meta_object_hash(meta_hash_hash, meta_object_hash={})
						new_hash = {}
						meta_hash_hash.keys.each do |key|
							# do we already have an object? yes -- use it. no -- create it.
							new_hash[key] = meta_object_hash[key] || new_hash[key] = column_meta_hash_to_meta_object(meta_hash_hash[key])
						end
						new_hash
					end
					def columns_meta_hash_set_to_meta_object_set(meta_hash_set, meta_object_hash={})
						(meta_hash_set.to_a.map { |item| (meta_object_hash[item.name] || column_meta_hash_to_meta_object(item))}).to_set()
					end
					def columns_meta_hash_array_to_meta_object_array(meta_hash_array, meta_object_hash=nil)
						new_array = []
						meta_hash_array.each do |column|
							# do we already have an object? yes -- use it. no -- create it.
							new_array << (meta_object_hash[column[:name]] || column_meta_hash_to_meta_object(column))
						end
						new_array
					end
				end
				class Data
					def initialize(hash, meta=nil, transform_params=true, init_nil=false, secure=true)
						@secure = secure
						h = nil
						h = init_nil_values(meta) if meta and init_nil
						if meta and transform_params
							thash = Db::Model::Params.transform(hash, meta.columns)
							if h
								@hash = h.freeze
								@mod = thash.dup
							else
								@hash = Db::Model::Params.transform(hash, meta.columns).freeze
							end
						else
							if h
								@hash = h.freeze
								@mod = hash.dup
							else
								@hash = hash.clone.freeze if hash
							end
						end
						@hash = {} unless @hash
						@mod = @hash.dup unless @mod
						@meta = meta
					end
					def _meta() @meta end
					def method_missing(m, *args, &block)
						if m == :[]=
							assign(args[0], args[1])
						end
						if m.to_s[-1] == '='
							#raise NoMethodError.new("undefined method #{m} for #{self.inspect}")
							key = m.to_s[0..-2].to_sym
							#puts "m: #{m}"
							#puts "key: #{key}"
							assign(key, args[0])
							return nil
						end
						if m == :[]
							return lookup(args[0])
						end
						if @hash.respond_to?(m)
							return @mod.send(m, *args, &block)
						end
						@mod[m]
					end
					def _primary_key
						primary_key = {}
						@meta.primary_key_columns.keys.each do |key|
							primary_key[key] = @mod[key]
						end
						primary_key
					end
					def _alternate_key
						alternate_key = {}
						@meta.alternate_key_columns.keys.each do |key|
							alternate_key[key] = @mod[key]
						end
						alternate_key
					end
					def _consume_hash(hash)
						@mod.each do |key, value|
							if hash.has_key?(key) and @mod[key] != hash[key]
								@mod[key] = hash[key]
							end
						end
					end
					def _values
						@mod
					end
					def _modified
						Set.new(_modified_hash.keys)
					end
					def _modified_hash
						hash = {}
						@mod.each do |key, value|
							hash[key] = value unless @mod[key] == @hash[key]
						end
						hash
					end
					def _has_value_hash
						hash = {}
						@mod.each do |key, value|
							hash[key] = value unless value == nil
						end
						hash
					end
					def _secure?
						@secure
					end
					def _secure(value)
						@secure = value
					end
					def to_hash
						#puts "#{_table_name} to_hash."
						return @mod.clone if @meta.secure_columns.length == 0 or !_secure?
						hash = {}
						puts "stripping secure columns!"
						@mod.each do |key, value|
							hash[key] = value unless @meta.secure_columns.has_key?(key)
						end
						hash
					end
					def to_h
						# ruby way :)
						to_hash
					end
					def to_json
						puts "to_json"
						json = Jungle::Json.dump to_hash
						puts json
						json
					end
					private
					def assign(key, value)
						@mod[key] = value
					end
					def lookup(key)
						@mod[key]
					end
					def init_nil_values(meta)
						hash = {}
						if meta
							meta.columns.keys.each do |key|
								hash[key] = nil
							end
						end
						hash
					end
				end
				class ZData
					def initialize(meta, hash=nil, mark_as_modified_if_changed=true, transform_params=false)
						@meta = meta
						@values = initialize_values()
						@secure = true
						@modified = Set.new # holds hash keys of modified values.
						hash = Db::Model::Params.transform(hash, @meta.columns) if transform_params
						_consume_hash(hash, mark_as_modified_if_changed) if hash
					end
					def _meta() @meta end
					def _consume_hash(hash, mark_as_modified_if_changed=true)
						@values.each do |key, value|
							if hash.has_key?(key) and @values[key] != hash[key]
								@values[key] = hash[key]
								@modified.add(key) if mark_as_modified_if_changed
							end
						end
					end
					def _values
						@values
					end
					def _modified
						@modified
					end
					def _modified_hash
						hash = {}
						@modified.each do |key|
							hash[key] = @values[key]
						end
						hash
					end
					def _has_value_hash
						hash = {}
						@values.each do |key, value|
							hash[key] = value unless value == nil
						end
						hash
					end
					def _primary_key
						primary_key = {}
						_primary_key_columns.keys.each do |key|
							primary_key[key] = @values[key]
						end
						primary_key
					end
					def _alternate_key
						alternate_key = {}
						_alternate_key_columns.keys.each do |key|
							alternate_key[key] = @values[key]
						end
						alternate_key
					end
					private
					def initialize_values
						values = {}
						@meta.columns.keys.each do |key|
							values[key] = nil
						end
						values
					end
				end
			end
		end
	end
end
