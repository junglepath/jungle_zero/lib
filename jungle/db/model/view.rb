require 'set'

module Jungle
	module Db
		module Model
			module View
				def self.view_meta_hash_to_meta_object(view_meta_hash)
					return Meta.new(view_meta_hash) if view_meta_hash
					nil
				end
				def self.transform_to_meta_view_hash(hash)
					if hash
						references = Set.new(hash[:references] || [])
						h = {
							create: hash[:create],
							drop: hash[:drop],
							references: references,
							parameters: hash[:parameters] || [],
							pre_query_hook: hash[:pre_query_hook]
						}
						return h
					end
					nil
				end
				class Meta
					def initialize(meta_view_hash)
						@hash = meta_view_hash
					end
					def create() @hash[:create] end
					def drop() @hash[:drop] end
					def references() @hash[:references] end
					def parameters() @hash[:parameters] end
					def pre_query_hook() @hash[:pre_query_hook] end
					def has_reference_to?(table_name) @hash[:references].member?(table_name) end
					def [](key) @hash[key] end
					def to_h() @hash end
					def to_hash() @hash end
				end
				def self.build_call(view, identity, table, from_clause_parameters=nil)
					# user identity parameters take precedence, then parameters coming with query. This prevents user supplied parameters
					# from being used to set identity parameters!
					parameters = identity.alternative_user_keys.select{|k, v| view.parameters.include?(k)}.values.map{|n| convert_to_parameter(n)}
					if from_clause_parameters and from_clause_parameters.length > 0
						from_clause_parameters.each do |p|
							parameters << p
						end
					end
					parameters = parameters.join(', ')
					if parameters.length > 0
						"#{table.table_name}(#{parameters})"
					else
						"#{table.table_name}"
					end
				end
				def self.run_pre_query_hook(view, identity, table, db, parameters=nil)
					view.pre_query_hook.call(identity, table, db) if view.pre_query_hook
				end
				def self.convert_to_parameter(value)
					return value if value.class == Fixnum
					return "'#{value}'" if value.class == String
					return value if value.class == Float
					return "'#{value.to_s}'::date'" if value.class == Date
					return "'#{value.to_time.utc}'::timestamp'" if value.class == DateTime
					return "'#{value.utc}'::timestamp'" if value.class == Time
					return value.to_s if value.class == TrueClass or value.class == FalseClass
					return "#{value}"
				end
			end
		end
	end
end
