module Jungle
	module Db
		module Model
			module Schema
				def self.get_sequel_method type, multi_column_pk=false
					return 'foreign_key' if type == :foreign_key
					return 'primary_key' if type == :primary_key and multi_column_pk == false
					return 'Integer' if type == :primary_key and multi_column_pk == true
					return 'String' if type == :string
					return 'Integer' if type == :integer
					return 'DateTime' if type == :timestamp
					return 'DateTime' if type == :timestamp_local
					return 'TrueClass' if type == :boolean
					return 'Date' if type == :date
					return 'json' if type == :json
					return 'jsonb' if type == :jsonb
					return 'Float' if type == :float
					return type
				end

				def self.get_postgresql_override_type type
					return 'text' if type == :string
					return 'integer' if type == :integer
					return 'bigint' if type == :bigint
					return 'timestamp' if type == :timestamp
					return 'timestamp' if type == :timestamp_local
					return 'boolean' if type == :boolean
					return 'date' if type == :date
					return 'json' if type == :json
					return 'jsonb' if type == :jsonb
					return 'double' if type == :float
					return type.to_s
				end

				def self.create_table db, table_meta
					# translate column type keys into Sequel methods used by Sequel (DB.create_table).

					# call the Sequel DB.create_table method with a block:
					#puts "\ttable_name: #{table_meta.table_name}"
					schema = self
					db.create_table? table_meta.table_name do
						made_pk = false
						table_meta.columns.each_value do |column|
							puts "\tcolumn: #{column.name} #{schema.get_sequel_method(column.type)} #{column.foreign_key_table_name}."
							method = schema.get_sequel_method(column.type, (table_meta.primary_key_columns.count > 1))
							args_hash = {}
							if column.foreign_key? and column.not_null?
								#args = [column.name, column.foreign_key_table_name, :null=>false]
								args = [column.name, column.foreign_key_table_name]
								args_hash[:null] = false
							elsif column.foreign_key?
								args = [column.name, column.foreign_key_table_name]
							elsif column.primary_key?
								args = [column.name]
								#made_pk = true
							else
								args = [column.name]
							end
							args_hash[:unique] = true if column.unique?
							args_hash[:null] = false if column.not_null?
							args_hash[:type] = schema.get_postgresql_override_type(column.override_type) unless column.override_type == nil
							args_hash[:default] = column.default_value unless column.default_value == nil
							args << args_hash
							puts "\t\tmethod: #{method}. args:#{args}.\n"
							made_pk = true if column.type == :primary_key and table_meta.primary_key_columns.count == 1 # should only happen when pk is single column.
							puts "column: #{column.name}, override_type: #{column.override_type}, override_type.class: #{column.override_type.class}, method: #{method}, args: #{args}."
							send(method, *args)

							#index:
							if column.index
								send('index', column.index)
							end

							#unique index:
							if column.unique_index
								send('unique', column.unique_index)
							end
						end

						if not made_pk
							#puts "creating primary_key for table #{table_meta.name}. keys: #{table_meta.primary_key_columns.keys}."
							send('primary_key', table_meta.primary_key_columns.keys, :name=>"#{table_meta.table_name}_pk".to_sym)
						end
						#puts "end of block"
					end
				end

				def drop_table db, table_meta
					db.drop_table? table_meta.table_name
				end
			end
		end
	end
end
