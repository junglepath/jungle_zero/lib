require 'oj'

Oj.default_options = {:mode => :compat, :symbol_keys => true, :indent=>2 }
module Jungle
  module Json
    def self.default_options=(option_hash)
      Oj.default_options = option_hash
    end

    #returns a json string
    def self.dump(object, options={})
      Oj.dump object, options
    end

    #returns a Ruby hash
    def self.load(json_string, options={})
      Oj.load(json_string, options)
    end

    def self.parse(json_string, options={})
      load(json_string, options)
    end

    def self.hi
      puts "hi from jungle/json! :)"
    end
  end
end
