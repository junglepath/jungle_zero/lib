class ::Time
  def to_json(*a)
    strftime "\"%Y-%m-%dT%H:%M:%S%z\""
  end
  def as_json(*a)
    strftime "\"%Y-%m-%dT%H:%M:%S%z\""
  end
end
