module Jungle
  module Query
    module Engine
      def run()
      end
      def parse()
      end
      
      module Run
        def self.run(query, db, level=7)
          dataset = run_query(query, db)
          rows = dataset_to_array(query, dataset) if level > 0
          rows = sort_by_ids(query, rows) if level > 1
          rows = transform_results_to_hash_array(query, rows) if level > 2
          rows = add_arrays(query, rows) if level > 3
          rows = combine_results(query, rows) if level > 4
          rows = sort_by_original_sort(query, rows) if level > 5
          rows = wrap_outer_objects(query, rows) if level > 6
          if rows
            if query.apply_limit_offset_to_sql
              return rows
            else
              puts "query.root.limit: #{query.root.limit}."
              puts "query.root.offset: #{query.root.offset}."
              limit = 0
              offset = 0
              if query.root.limit and query.root.limit > 0
                limit = query.root.limit
              end
              if query.root.offset and query.root.offset > 0
                offset = query.root.offset
              end
              puts "rows.length: #{rows.length}."
              return rows if limit == 0 and offset == 0
              if offset > 0 and limit == 0
                rows = rows[offset, -1]
                rows = [] unless rows
                return rows
              end
              rows = rows[offset, limit]
              rows = [] unless rows
              return rows
            end
          else
            return dataset
          end
        end
  
        def self.run_query(query, db)
          ds = db[query.sql, *query.values]
        end
        
        def self.dataset_to_array(query, dataset)
          results = []
          dataset.each do |row|
            results << row
          end
          results
        end
  
        def self.sort_by_ids(query, results)
          keys = query.sort_ids
          if results.length > 0
            results = results.sort_by {|h| (h.reject {|x| !keys.include?(x)}).values }
          end
          results
        end
  
        def self.calc_paths(query)
          # For each sort field, find the path to its value in the nested hash.
          # Returns an array containing an arrays of hash keys. (One array of keys for each sort field.)
          sort_fields = query.root.sort
          paths = []
          sort_fields.each do |sort_field|
            keys = []
            paths << keys
            if sort_field.entities
              sort_field.entities.each do |e|
                keys << e.to_sym
              end
            end
            keys << sort_field.field_name
          end
          paths
        end
  
        def self.calc_sort_orders(query)
          # For each sort field, add its ordering (:asc or :desc) into the returned array.
          sort_fields = query.root.sort
          sort_orders = []
          sort_fields.each do |sort_field|
            sort_orders << sort_field.sort.to_sym
          end
          sort_orders
        end
  
        def self.wrap_outer_objects(query, rows)
          rows.map {|row| {query.root.name => row}}
        end
  
        def self.sort_by_original_sort(query, rows)
          if query.root.sort and query.root.sort.length > 0
            paths = calc_paths(query)
            sort_orders = calc_sort_orders(query)
            sorter = Jungle::Query::NestedHashSorter.new(paths, sort_orders)
            begin
              #strange: when I added this debugging code, modes_of_action.uql sorting on code started working...
              #was getting: ArgumentError - comparison of Hash with Hash failed.
              #very strange!
              count = 0
              rows = rows.sort do |a, b|
                count += 1
                val = sorter.sort(a, b)
                val
              end
            rescue
              puts "count: #{count}."
              raise
            end
          end
          rows
        end
  
        def self.transform_results_to_hash_array(query, rows)
          # transform results into nested hashes (for converting to json later...)
          results = []
          rows.each do |row|
            #binding.pry
            a_stack = []
            h_stack = []
            current_alias = nil
            current_hash = nil
            row.each do |k, v| #each item in this row...
              new_alias, field_name = k.to_s.split(".").map{|a| a.to_sym}
              parent_alias = query.aliases[new_alias].parent_alias
              #parent_alias = parent_alias.to_sym if parent_alias
  
              #load stack:
              if current_alias
                a_stack.push(current_alias)
                h_stack.push(current_hash)
              end
  
              #unload stack:
              index = a_stack.index(new_alias)
              if index
                # We've seen this new alias before. Get it (and the related hash) out of the stacks:
                begin
                  current_alias = a_stack.pop
                  current_hash = h_stack.pop
                end until current_alias == new_alias
                current_hash[field_name] = v
  
              elsif current_alias
                if a_stack.include?(parent_alias)
  
                  #get parent hash
                  begin
                    temp_parent_alias = a_stack.pop
                    temp_parent_hash = h_stack.pop
                  end until temp_parent_alias == parent_alias
  
                  # put new hash into parent hash:
                  new_hash = {}
                  temp_parent_hash[query.aliases[new_alias.to_sym].name ] = new_hash # (convert alias to entity name)
                  a_stack.push temp_parent_alias
                  h_stack.push temp_parent_hash
  
                  # we now have new currents:
                  current_alias = new_alias
                  current_hash = new_hash
                  current_hash[field_name] = v
                else
                  raise "parent_alias: #{parent_alias} was not found in a_stack: #{a_stack}. (h_stack: #{h_stack}).\nresults:\n#{results}."
                end
              else
                #completely new:
                current_alias = new_alias
                current_hash = {}
                current_hash[field_name] = v
              end
            end
            # get the earliest parent (which contains all of the rest) and add to results:
            while h_stack.length > 0
              current_hash = h_stack.pop
            end
            results << current_hash
            #break # <= for debugging only
          end
          results
        end
  
        def self.add_arrays(query, rows)
          results = []
          rows.each do |hash|
            symbols = query.symbols.reverse
            results << add_array(symbols, hash)
          end
          results
        end
  
        def self.add_array(symbols, hash)
          result = {}
          hash.each do |name, value|
            if value.class == Hash
              symbol = symbols.pop
              if symbol == "<="
                result[name] = []
                result[name] << add_array(symbols, value)
              else
                result[name] = add_array(symbols, value)
              end
            else
              result[name] = value
            end
          end
          result
        end
  
        def self.primary_keys_match?(row_a, row_b, pk_fld_count)
          # most common:
          if pk_fld_count == 1
            if row_a.first[1] == row_b.first[1]
              return true
            end
          end
  
          # multiple PKs:
          values_a = row_a.values
          values_b = row_b.values
          values_a.each_index do |i|
            if i == pk_fld_count
              break
            end
            if values_a[i] != values_b[i]
              return false
            end
          end
          return true
        end
  
        def self.combine_results(query, rows)
          results = []
  
          aliases = {}
          query.aliases.values.each do |ai|
            aliases[ai.name] = ai
          end
  
          rows.each do |new_row|
            if results.length == 0
              results << new_row
            else
              begin
                previous_row = results[-1]
                if primary_keys_match?(previous_row, new_row, aliases.values[0].primary_key_columns_count)
                  #merge the row...
                  results[-1 ] = merge_hash(previous_row, new_row, aliases)
                else
                  # add the row...
                  results << new_row
                end
              rescue
                puts "rescue..."
                puts previous_row
                puts ""
                puts new_row
                puts ""
                raise
              end
            end
          end
          results
        end
  
        def self.merge_hash(a_hash, b_hash, aliases) #pk_fld_counts)
          h = {}
          a = a_hash.to_a # [[name, value], [name, value], ... ]
          b = b_hash.to_a # [[name, value], [name, value], ... ]
          a.each_index do |i|
            a_key = a[i][0] # name
            a_value = a[i][1] # value
            b_value = b[i][1] # value
            if a_value.class == Array
  
              pk_fld_count = aliases[a_key].primary_key_columns_count
  
              h[a_key] = a_value # a array
              a_hash = a_value[-1] # do we merge last element?
              b_hash = b_value[0] # compare to value from new row...
              if primary_keys_match?(a_hash, b_hash, pk_fld_count)
                h[a_key][-1] = merge_hash(a_hash,  b_hash, aliases)
              else
                h[a_key] << b_hash # add value from new row to array
              end
            elsif a_value.class == Hash
              pk_fld_count = aliases[a_key].primary_key_columns_count
  
              if primary_keys_match?(a_value, b_value, pk_fld_count)
                h[a_key] = merge_hash(a_value, b_value, aliases)
              else
                raise "unexpected multiple values found!!! Expected\na_value: '#{a_value}'\npk to match\nb_value: '#{b_value}'\npk."
              end
            else
              # regular field so a_value and b_value are the same.
              h[a_key] = a_value
            end
          end
          h
        end
      end

      module Tokens
        def self.initialize(string)
          tokens = tokenize(string)
          #tokens = remove_commas(tokens)
          tokens = tokens.reverse # reverse so I can 'pop' in original order.
        end

        def self.next(tokens)
          token = tokens.pop
        end

        def self.peek(tokens)
          token = tokens[-1]
        end

        def self.remove_commas(tokens)
          tokens.select{|t| t != ","}
        end
        
        def self.is_name?(token)
          token =~ /^[a-zA-Z_]/ # starts with alpha or underscore
        end

        def self.is_entity?(token, ptoken)
          is_name?(token) && (ptoken == ':' || ptoken == '(')
        end

        def self.tokenize(query)
          tokens = []
          in_single_line_comment = nil
          in_comment = 0
          in_string = nil
          in_escape = false
          parens = 0
          in_bracket = false
          token = ""
          i = 0
          prev_char = nil
          next_char = nil
          query.each_char do |c|
            i += 1
            next_char = query[i]
            #puts "i: #{i}, c: #{c}, tokens: #{tokens}"
            if in_escape and c == "\\"
              in_escape = false
              token << "\\"
            elsif in_escape and c == "t"
              in_escape = false
              token << "\t"
            elsif in_escape and c == "n"
              in_escape = false
              token << "\n"
            elsif in_escape and c == "r"
              in_escape = false
              token << "\r"
            elsif in_escape and c == "\'"
              in_escape = false
              token << "\'"
            elsif in_escape and c == "\""
              in_escape = false
              token << "\""
            elsif in_escape
              raise "Invalid character #{c} was escaped. You can only escape as follows: \\\\ \\t \\n \\r \\\' \\\" got it? :)"
            elsif in_string and c.strip.empty? # is whitespace?
              token << c
            elsif in_string and in_string == c
              token = token << c
              tokens << token
              token = ""
              in_string = nil
            elsif in_string and c == "\\"
              in_escape = true
            elsif in_string
              token << c
            elsif in_comment > 0 and c == "*"
              #do nothing
            elsif in_comment > 0 and prev_char == "*" and c == "/"
              in_comment -= 1
            elsif in_single_line_comment and (c == "\n" or c == "\r")
              in_single_line_comment = false
            elsif in_single_line_comment
              #do nothing
            elsif c == "/" and next_char == "*"
              in_comment += 1
            elsif in_comment > 0
              #do nothing
            elsif c == "-" and next_char == "-"
              if token.length > 0
                tokens << token
                token = ""
              end
              in_single_line_comment = true
            #elsif in_bracket and c == "]"
            #  in_bracket = false
            #  token << c
            #  tokens << token
            #  token = ""
            #elsif in_bracket
            #	token << c
            elsif c == "\""
              if token.length > 0
                tokens << token
                token = ""
              end
              in_string = c
              token << c
            elsif c == "'"
              if token.length > 0
                tokens << token
                token = ""
              end
              in_string = c
              token << c
            elsif c.strip.empty? # is whitespace?
              if token.length > 0
                tokens << token
                token = ""
              end
            elsif c == ","
              if token.length > 0
                tokens << token
                token = ""
              end
              #tokens << "," #ignore commas outside of strings?
            elsif c == ":" # count ':' as a token outside of strings.
              if token.length > 0
                tokens << token
                token = ""
              end
              tokens << ":"
            elsif c == "("
              parens += 1
              if token.length > 0
                tokens << token
                token = ""
              end
              tokens << "("
            elsif c == ")"
              parens -= 1
              if parens < 0
                raise "Extra ')' character found at #{i}."
              end
              if token.length > 0
                tokens << token
                token = ""
              end
              tokens << ")"
            elsif c == "["
              in_bracket = true
              if token.length > 0
                tokens << token
                token = ""
              end
              #token << c
              tokens << "["
            elsif in_bracket and c == "]"
              in_bracket = false
              if token.length > 0
                tokens << token
                token = ""
              end
              tokens << "]"
            else
              token << c
            end
            prev_char = c
          end
          if in_bracket
            raise "No closing ']' character was found."
          end
          if parens > 0
            raise "Missing #{parens} ')' characters."
          end
          if in_string
            raise "Unclosed string: missing #{in_string} character."
          end
          if token.length > 0
            tokens << token
            token = ""
          end
          tokens
        end
      end

      module Nodes
        Tokens = ::Jungle::Query::Engine::Tokens

        EntityNode = Struct.new(:type, :parent, :table, :aka, :key, :fields, :children, :sql_alias, :filter, :sort, :limit, :offset, :parameters, keyword_init: true) do
          def to_s()
            {entity_node: to_h}.to_s
          end
        end

        FieldNode = Struct.new(:type, :column, keyword_init: true) do
          def to_s()
            {field_node: to_h}.to_s
          end
        end

        SortNode = Struct.new(:string_value, :field_name, :path, :sort, keyword_init: true) do
          def to_s()
            string_value
          end
        end

        def self.get_entity_node(ers, tokens, token, parent, sql_aliases)
          node = nil
          ptoken = Tokens.peek(tokens)
          #puts "parent: #{parent.table.name}." if parent
          #puts "token: #{token}, ptoken: #{ptoken}."
          raise "Error: expected: '(' found: '#{ptoken}'." if ptoken == ':' && parent == nil
          node = get_root_node(ers.models, token) unless parent
          node = get_up_linked_child_node(ers, tokens, token, parent) unless node if ptoken == ':'
          node = get_down_linked_child_node(ers, tokens, token, parent) unless node if ptoken == '('
          node.sql_alias = sql_aliases.pop
          node
        end

        def self.get_field_node(token, entity)
          field_name = get_field_name_from_token(token)
          field = entity.table.columns[field_name]
          raise "Error: field '#{field_name}' from token '#{token}' not found for entity '#{entity.table.name}'." unless field
          FieldNode.new(type: 'field', column: field)
        end

        def self.get_limit_or_offset_primitive(input_string)
          value = input_string.strip
          return value.to_i if value =~ /^[0-9]*$/
          raise "Found invalid limit or offset: #{input_string}. Limits and offsets must be integers. Such as [10]."
        end

        def self.get_sort_node(link_key, field, root_entity)
          entity_node = nil
          path = nil
          if link_key
            entity_node, path = sort_path(link_key, root_entity)
            raise "entity reference '#{link_key}' in sort clause is not a valid reference from your query." unless entity_node
          else
            entity_node = root_entity
            path = [root_entity]
          end
          field_node = entity_node.fields.select{|f| f.column.name == field}[0]
          raise "Field #{field} is not a valid field for entity #{entity_node.table.name} in your query," unless field_node
          string_value = []
          string_value << entity_node.aka if entity_node.aka
          string_value << field
          SortNode.new(string_value: "#{string_value.join('.')}", field_name: field, path: path, sort: 'asc')
        end

        private

        def self.confirm_child_foreign_key_alias_points_to_parent(parent, child, aka)
          fk = child.foreign_key_columns_by_table_alias[aka]
          raise "Error: foreign_key_columns_by_table_alias not found for table alias: '#{aka}' on child: '#{child.name}' of parent: '#{parent.table.name}'." unless fk
          parent_name = fk.values[0].foreign_key_table_name
          #puts "parent_name: #{parent_name}."
          raise "child: #{child.name} has no foreign key reference to parent: #{parent.name}." unless parent_name && parent_name == parent.table.name
          true
        end

        def self.sample_fks(table, token, parent_name)
          akas = table.foreign_key_columns_by_table_alias.keys.select{|k| table.foreign_key_columns_by_table_alias[k].values[0].foreign_key_table_name == parent_name}
          akas.map{|aka| "    '#{token}:#{aka}'"}.join(" or\n")
        end

        def self.find_entity(models, token)
          #puts "find_entity: token: #{token}."
          raise "expected token to be an entity name (starts with alpha or underscore). token: #{token}" unless Tokens.is_name?(token)
          e = models[token.to_sym] # token should match child table name.
          #puts e.class if e
          raise "entity '#{token}' not found." unless e
          e
        end

        def self.get_alias(tokens)
          t = Tokens.next(tokens) # next token should be a ':'
          raise "Error: expected next token to equal: ':', but found: '#{t}'." unless t == ':'
          a = Tokens.next(tokens) # next token should be an fk columns table alias defined on child.
          raise "Error: expected remote alias to start with alpha (a-zA-Z) or underscore (_) character, but found: '#{a}." unless Tokens.is_name?(a)
          a.to_sym
        end

        def self.get_field_name_from_token(token)
          if token[-1] == "@"
            token = token[0..-2]
          end
          if token.include? '.'
            token = token.split('.')[0]
          end
          token = token.downcase.to_sym
        end

        def self.get_root_node(models, token)
          e = find_entity(models, token)
          EntityNode.new(type: '!', table: e)
        end

        def self.get_up_linked_child_node(ers, tokens, token, parent)
          # find entity linked by parent pk (to child fk)
          #
          # example query syntax:
          # ...user(id, name, user_role:user(user_id, role_id))
          #
          ulinks = ers.ulinks[parent.table.name]
          raise "up_linked child nodes not found for parent entity: #{parent.table.name}." unless ulinks
          colon = Tokens.next(tokens)
          raise "expected token: ':' found: '#{colon}'" unless colon && colon == ':'
          aka = Tokens.next(tokens)
          raise "expected token to be a name (starts with alpha or underscore). found: '#{aka}'" unless Tokens.is_name?(aka)
          key = "#{token}:#{aka}"
          child_name = ulinks[key]
          raise "entity reference not found\n\n  '#{key}' was not found on: #{parent.key || parent.table.name}.\n\nmaybe you meant one of these?\n\n#{link_keys(ers, parent)}\n\n." unless child_name
          child = find_entity(ers.models, child_name)
          EntityNode.new(type: '<', parent: parent, table: child, aka: aka.to_sym, key: key)
        end

        def self.link_keys(ers, parent)
          dlinks = ers.dlinks[parent.table.name] || {}
          ulinks = ers.ulinks[parent.table.name] || {}
          links = dlinks.keys + ulinks.keys
          links.map{|l| "    #{l}\n"}.join()
        end

        def self.get_down_linked_child_node(ers, tokens, token, parent)
          #
          # find entity linked by parent fk (to child pk)
          # user(id, name, location(id, name), previous_location(id, name), ...)
          #
          dlinks = ers.dlinks[parent.table.name]
          raise "down_linked child nodes not found for parent entity: #{parent.table.name}." unless dlinks
          child_name = dlinks[token.to_sym]
          raise "entity reference not found\n\n  '#{token}' was not found on: #{parent.key || parent.table.name}.\n\nmaybe you meant one of these?\n\n#{link_keys(ers, parent)}\n\n." unless child_name
          child = find_entity(ers.models, child_name)
          EntityNode.new(type: '>', parent: parent, table: child, aka: token.to_sym, key: token.to_sym)
        end

        def self.sort_path(link_key, parent, path:nil)
          path ||= [parent]
          node = nil
          parent.children.each do |child|
            path << child
            node = child if child.key == link_key
            break if node
            node, path = sort_path(link_key, child, path: path)
            break if node
            path.pop
          end
          return node, path
        end

      end

      module Lang
        def self.operators
          Set.new(["==", "!=", ">", "<", ">=", "<=", "~", "=~", "~~", ">>", "<<", "is", "is!"])
        end
  
        def self.operator_map
          {"==" => "=", "!=" => "!=", ">" => ">", "<" => "<", ">=" => ">=", "<=" => "<=", "~" => "ilike", "=~" => "regex", "~~" => "full text search", ">>" => "in", "<<" => "not in", "is" => "is", "is!" => "is not"}
        end
  
        def self.connectors
          Set.new(["and", "or"])
        end
  
        def self.sorts
          Set.new(["asc", "desc"])
        end

        def self.parse_float(value)
          value.to_f
        end

        def self.parse_int(value)
          value.to_i
        end

        def self.parse_operator(value)
          operator_map[value]
        end

        def self.parse_string(value)
          #also used for date literals
          value[1..-2] #strip out beginning and ending quotes.
        end
      end

      module Parse
        Lang = ::Jungle::Query::Engine::Lang
        Nodes = ::Jungle::Query::Engine::Nodes
        Tokens = ::Jungle::Query::Engine::Tokens

        def self.query(ers, query, apply_limit_offset_to_sql: false)
          tokens = Tokens.initialize(query)
          sql_aliases = initialize_sql_aliases()
          root, values = read_entity(ers, sql_aliases, tokens, nil, nil, values: [])
          #sql, aliases, symbols, sort_ids, primary_key_field_count = Jungle::Query::SQLString.generate( self, (root) )
          #Jungle::Query::Query.new(root, sql, values, aliases, symbols, sort_ids, primary_key_field_count, apply_limit_offset_to_sql)
          return root, values
        end

        def self.read_entity(ers, sql_aliases, tokens, token, parent, values:, in_fields: false)
          expect_entity = false
          expect_fields = false
          expect_filter = false
          expect_sort = false
          expect_limit = false
          expect_offset = false
          entity = nil
          token = Tokens.next(tokens) unless token
          while token
            if !entity
              expect_entity = true
              node = Nodes.get_entity_node(ers, tokens, token, parent, sql_aliases)
              entity = node
              root_entity ||= node
              expect_entity = false
              expect_fields = true
            elsif expect_offset and token == "[" #token.start_with? "["
              expect_offset = false
              entity.offset = read_limit_or_offset(tokens) # same as limit
            elsif expect_limit and token == "[" #token.start_with? "["
              expect_limit = false
              entity.limit = read_limit_or_offset(tokens)
              expect_offset = true
            elsif expect_sort and token == "("
              raise "Sorts need to go in outer query -- they cannot be nested. Qualify fields using entity names. No qualification is needed for the outer entity." if in_fields
              expect_sort = false
              entity.sort = read_sort(tokens, sql_aliases, root_entity)
              break unless Tokens.peek(tokens) == "["
              expect_limit = true
            elsif expect_filter and token == "("
              expect_filter = false
              entity.filter, values = read_filter(entity, tokens, sql_aliases, values, root_entity)
              break unless Tokens.peek(tokens) == '('
              expect_sort = true
            elsif expect_fields and token == "[" # got parameters which should be passed to the entity in sql as a table function.
              entity.parameters = process_parameters(tokens)
            elsif expect_fields and token == "("
              expect_fields = false
              entity, values = read_fields(ers, entity, tokens, sql_aliases, values: values)
              break unless Tokens.peek(tokens) == '('
              expect_filter = true
            else
              raise "Unexpected token: #{token}."
            end
            token = Tokens.next(tokens)
          end
          return entity, values
        end

        def self.read_fields(ers, entity, tokens, sql_aliases, values:)
          fields = entity.fields || []
          fpresent = {}
          fields.each do |f|
            fpresent[f.column.name] = f
          end

          children = entity.children || []
          cpresent = {}
          children.each do |c|
            cpresent[c.aka] = c
          end

          token = Tokens.next(tokens)
          ptoken = Tokens.peek(tokens)
          raise "Error: expected ')' not found." unless token
          while token
            if token == ")" && fields.length == 0
              # Add all fields
              fields = add_all_fields(entity, fields)
              break
            elsif token == ")"
              break
            elsif token == '*' && fields.length == 0 && ptoken == ')'
              fields = add_all_fields(entity, fields)
            elsif token == '*' && fields.length == 0 && Tokens.is_name?(ptoken)
              fields = add_all_fields(entity, fields)
            elsif Tokens.is_entity?(token, ptoken)
              node, values = read_entity(ers, sql_aliases, tokens, token, entity, values: values, in_fields: true)
              raise "Error: duplicate child entity '#{node.aka}' specified for parent entity '#{entity.table.name}'." if cpresent[node.aka]
              children << node
              cpresent[node.aka] = node
            elsif Tokens.is_name?(token)
              # Find field
              node = Nodes.get_field_node(token, entity)
              #puts "fnode:"
              #puts node.type
              #puts node.column.name
              raise "Error: duplicate field '#{node.column.name}' specified for entity '#{entity.table.name}'." if fpresent[node.column.name]
              fields << node
              #puts "fields.class: #{fields.class}."
              fpresent[node.column.name] = node
            else
              raise "Error: Invalid token '#{token}' found in field list: #{fields.map{f.column.name}}."
            end
            token = Tokens.next(tokens)
            ptoken = Tokens.peek(tokens)
          end
          # Ensure PK fields are included
          entity.table.primary_key_columns.values.each do |pkc|
            fields << field_node.new(type: 'field', column: pkc) unless fpresent[pkc.name]
          end
          entity.fields = fields
          entity.children = children
          #puts "entity.fields.class: #{entity.fields.class}."
          return entity, values
        end

        def self.read_filter(entity, tokens, aliases, values, root_entity)
          #puts "process_filter..."
          filter = []
          nest_count = 0
          expect_field = true
          expect_value = false
          expect_operator = false
          expect_list = false
          expect_connector = false
          expect_comma = false
          token = Tokens.next(tokens)
          #puts "token: #{token}"
          while token
            if expect_operator and Lang.operators.include? token
              expect_operator = false
              filter << Lang.parse_operator(token)
              if token == ">>" or token == "<<"
                expect_list = true
              else
                expect_value = true
              end
            elsif expect_operator
              raise "Expected token #{token} to be one of the following operators: #{Lang.operators.inspect}."
            elsif expect_field and token == "("
              nest_count += 1
              filter << token
            elsif expect_field and token == ")" and nest_count > 0
              nest_count -= 1
              filter << token
            elsif expect_field and token == ")"
              break
            elsif expect_value and token == 'null'
              # values << token #skip this token will use the literal null in filter.
              filter << "null"
              expect_value = false
              expect_field = true
              expect_connector = true
            elsif expect_value and token =~ /^[0-9]/ # starts with number
              if token.include? "."
                values << Lang.parse_float(token)
                filter << "?"
              else
                values << Lang.parse_int(token)
                filter << "?"
              end
              expect_value = false
              expect_field = true
              expect_connector = true
            elsif expect_value and token =~ /^["']/ # starts with quote
              values << Lang.parse_string(token)
              filter << "?"
              expect_value = false
              expect_field = true
              expect_connector = true
            elsif expect_value and token.downcase == "true"
              values << true
              filter << "?"
              expect_value = false
              expect_field = true
              expect_connector = true
            elsif expect_value and token.downcase == "false"
              values << false
              filter << "?"
              expect_value = false
              expect_field = true
              expect_connector = true
            elsif expect_list and token == "("
              filter << "("
              filter << process_value_list(entity, tokens, aliases, root_entity, values)
              expect_list = false
              expect_field = true
            elsif expect_field and expect_connector and Lang.connectors.include? token.downcase
              filter << token.downcase
              expect_connector = false
            elsif expect_field and Lang.connectors.include? token.downcase
              raise "Did not expect connector token #{token}."
            elsif expect_field and Tokens.is_name?(token)
              node = Nodes.get_field_node(token, entity)
              filter << "#{entity.sql_alias}.#{node.column.name}"
              expect_field = false
              expect_connector = false
              expect_operator = true
            else
              raise "Token #{token} is not a valid column name. filter: #{filter.inspect}; tokens: #{tokens.inspect}."
            end
            token = Tokens.next(tokens)
          end
          return filter, values
        end

        def self.read_limit_or_offset(tokens)
          limit = nil
          limit_contents = []
          token = Tokens.next(tokens)
          while token
            if token == "]"
              limit = Nodes.get_limit_or_offset_primitive(limit_contents.join)
              break
            else
              limit_contents << token
            end
            token = Tokens.next(tokens)
          end
          limit
        end

        def self.read_sort(tokens, aliases, root_entity)
          sort = []
          sort_field = nil
          expect_sort = false
          token = Tokens.next(tokens)
          while token
            #puts "token: #{token}."
            if token == ")"
              break
            elsif token == ","
              #do nothing
              #sort << ", "
            elsif expect_sort and Lang.sorts.include? token.downcase # :asc, or :desc
              expect_sort = false
              sort[-1].sort = token.downcase
            elsif Tokens.is_name?(token)
              link_key, field = read_sort_link(token, tokens)
              sort << Nodes.get_sort_node(link_key, field, root_entity)
              expect_sort = true
            else
              raise "token #{token} is not a valid table nor column name (or combination)."
            end
            token = Tokens.next(tokens)
          end
          #puts "sort: #{sort.map{|s| s.path[-1].table.name.to_s + ' ' + s.string_value + ' ' + s.sort}}."
          sort
        end

        def self.read_sort_link(token, tokens)
          parts = token.downcase.split('.')
          if parts.length > 1
            link_key = parts[0]
            field = parts[1].to_sym
          elsif Tokens.peek(tokens) == ':'
            Tokens.next(tokens) # throw away ':'
            cparts = Tokens.next(tokens).split('.')
            link_key = "#{token}:#{cparts[0].to_sym}"
            field = cparts[1].to_sym
          else
            link_key = nil
            field = parts[0].to_sym
          end

          return link_key, field
        end
        
        def self.add_all_fields(entity, fields)
          entity.table.columns.values.each do |c|
            fields << field_node.new(type: 'field', column: c)
          end
          fields
        end

        def self.initialize_sql_aliases()
          aliases = ('a'..'z').to_a.reverse
          doubles = aliases.map{|a| a + a} #also add in "aa", "bb", ... "zz"
          aliases = doubles.push(*aliases)
        end

        def zparse_tokens(root, tokens)
          aliases = ('a'..'z').to_a.reverse
          doubles = aliases.map{|a| a + a} #also add in "aa", "bb", ... "zz"
          aliases = doubles.push(*aliases)
          values = []
          entity_root = process_entity(root, nil, nil, tokens, aliases, values)
          return entity_root, values
        end
      end

      module Test
        Parse = ::Jungle::Query::Engine::Parse

        def self.parse(ers, query)
          result, values = Parse.query(ers, query)
          return result, values
        end
      end
    end
  end
end
