# query.rb
#require 'pry-byebug'
#require 'pp'
#use: binding.pry where you want a break point.
require 'set'

module Jungle
  module Query
    class From
      attr_reader :join_text, :table_name, :table_alias, :on_a_column_name, :on_b_alias, :on_b_column_name, :parameters
      attr_accessor :table_replacement_text
      def initialize join_text, table_name, table_alias, on_a_column_name=nil, on_b_alias=nil, on_b_column_name=nil, parameters=nil
        @join_text = join_text
        @table_name = table_name.to_sym
        @table_alias = table_alias
        @on_a_column_name = on_a_column_name
        @on_b_alias = on_b_alias
        @on_b_column_name = on_b_column_name
        @table_replacement_text = nil
        @parameters = parameters
      end

      def to_s
        table_name = @table_replacement_text || @table_name
        table_name = "\"#{table_name}\"" unless table_name.class == String and table_name.include?('(')
        if join_text == "" or join_text == nil
          "#{table_name} #{@table_alias}"
        else
          "  #{@join_text} #{table_name} #{@table_alias} on #{@table_alias}.#{@on_a_column_name} = #{@on_b_alias}.#{@on_b_column_name}"
        end
      end

      def to_str
        to_s
      end
    end
  end
end
