# query.rb
#require 'pry-byebug'
#require 'pp'
#use: binding.pry where you want a break point.
require 'set'
#require 'jungle/db/model/view'
#require 'jungle/query/alias_info'

module Jungle
  module Query
    module Sql
      module Context
        def self.environment
          @environment ||= Struct.new(:ers, :apply_limit_offset_to_sql, :identity, keyword_init: true)
        end
      end
      module Nodes
        From = Struct.new(:table_replacement_text, :join_text, :table_name, :table_alias, :on_a_column_name, :on_b_alias, :on_b_column_name, :parameters, keyword_init: true) do
          def to_s()
            t_name = table_replacement_text || table_name
            t_name = "\"#{t_name}\"" unless t_name.class == String and t_name.include?('(')
            return "#{t_name} #{table_alias}" if join_text == "" or join_text == nil
            "  #{join_text} #{t_name} #{table_alias} on #{table_alias}.#{on_a_column_name} = #{on_b_alias}.#{on_b_column_name}"
          end
        end
        AliasInfo = Struct.new(:name, :alias_, :parent_alias, :primary_key_columns_count, keyword_init: true) do
          def name
            return @name.to_sym if @name
          end
          def alias_
            return @alias_.to_sym if @alias
          end
          def parent_alias
            return @parent_alias.to_sym if @parent_alias
          end
        end
      end

      def self.generate(engine, entity, select=nil, from=nil, where=nil, sort=nil, sort_ids=nil, aliases=nil, symbols=nil, primary_key_field_count=nil)
        primary_key_field_count = [] unless primary_key_field_count
        aliases = {} unless aliases
        symbols = [] unless symbols
        outer = true unless select
        select = [] unless select
        from = [] unless from
        where = [] unless where
        sort = [] unless sort
        sort_ids = [] unless sort_ids # need to sort sql query in id order for each table with fields in select list. This is so objects with same ID can be combinded.
        sort_original = [] unless sort_original

        generate_from(from, entity, aliases, engine.tables, symbols)
        generate_fields(engine, entity, select, from, where, sort, sort_ids, aliases, symbols, primary_key_field_count)
        generate_filter(entity, where)
        generate_sort(entity, sort)

        indent = "\n  "

        if outer
          hook_in_filters engine, select, from, where, sort, entity
          hook_in_table_filters engine, select, from, where, sort, entity
          sql = generate_sql(select, from, where, sort, entity, engine.apply_limit_offset_to_sql)
          return sql, aliases, symbols, sort_ids, primary_key_field_count
        else
          return select, from
        end
      end

      def self.generate_from(from, entity, aliases, tables, symbols)
        if from.length == 0
          from << Nodes::From.new(table_name: entity.table.name, table_alias: entity.sql_alias, parameters: entity.parameters)
          aliases[entity.sql_alias.to_sym] = AliasInfo.new(name: entity.table.name, alias_: entity.sql_alias.to_sym, parent_alias: nil, primary_key_field_count: entity.table.primary_key_columns.count)
        else
          if entity.left_join
            join_text = "left join"
          else
            join_text = "join"
          end
          from << Nodes::From.new(join_text: join_text, entity.table.name, entity.sql_alias, entity.node.child_table_join_column_name, entity.parent.alias_, entity.node.parent_table_join_column_name, entity.parameters)
          aliases[entity.sql_alias.to_sym] = AliasInfo.new(entity.name, entity.alias_.to_sym, entity.find_parent_alias, tables[entity.node.child_table_name.to_sym].primary_key_columns.count)
            #puts "aliases: #{aliases}."
          if entity.fields.count > entity.fields_that_are_entities_count
            symbols << entity.find_symbol
          end
        end
      end

      def self.generate_fields(engine, entity, select, from, where, sort, sort_ids, aliases, symbols, primary_key_field_count)
        if entity.fields and entity.fields.length > 0
          entity.fields.each do |field|
            if field.is_entity?
              select, from = generate(engine, field, select, from, where, sort, sort_ids, aliases, symbols, primary_key_field_count)
            else
              if field.is_primary_key_column?
                sort_ids << "#{entity.alias_}.#{field.name}".to_sym
              end
              puts "#{entity.alias_}.#{field.name} as \"#{entity.alias_}.#{field.name}\" is_secure? #{field.is_secure?}."
              if !field.is_secure? or (field.is_secure? and engine.identity.authorization_filter.is_root? or engine.identity.authorization_filter.is_user_admin?)
                select << "#{entity.alias_}.#{field.name} as \"#{entity.alias_}.#{field.name}\""
              end
            end
          end
          #puts "entity.name: #{entity.name}."
        #else
        #  select = "*"
        end
      end

      def self.generate_filter(entity, where)
        if entity.filter and entity.filter.length > 0
          where << "and" if where.length > 0
          entity.filter.each do |filter|
            where << filter
          end
        end
      end

      def self.generate_sort(entity, sort)
        if entity.sort and entity.sort.length > 0
          #sort << ", " if sort.length > 0
          entity.sort.each do |s|
            sort << s.value
          end
        end
      end

      def self.hook_in_filters(engine, select, from, where, sort, entity)
        puts "hook_in_filters QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFf"
        #binding.pry
        if engine.identity and engine.identity.query_filters
          engine.identity.query_filters.each do |filter|
            puts "hook_in_filters: filter.table_name: #{filter.table_name}."
            from.each do |frm|
              puts "hook_in_filters: frm.table_name: #{frm.table_name}."
              if frm.table_name == filter.table_name
                table = engine.tables[frm.table_name]
                where << "and" if where.length > 0
                where << filter.fk_in_query(frm.table_alias, table.primary_key_columns.values[0].name)
              # don't think I need this part:
              #else
              #  table = engine.tables[frm.table_name]
              #  table.foreign_key_columns.values.each do |column|
              #    if column.foreign_key_table_name == filter.table_name
              #      where << "and" if where.length > 0
              #      where << filter.fk_in_query(frm.table_alias, column.name)
              #    end
              #  end
              end
            end
          end
        end
      end

      def self.hook_in_table_filters(engine, select, from, where, sort, entity)
        puts "hook_in_table_filters QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFf"
        #binding.pry
        if engine.identity and engine.identity.table_filters
          from.each do |frm|
            filter = engine.identity.table_filters[frm.table_name]
            if filter
              puts "hook_in_table_filters: frm.table_name: #{frm.table_name}."
              replacement_table = engine.tables[filter[:replacement]]
              puts "replacement_table: #{replacement_table.name}"
              table_replacement_text = nil
              if replacement_table.view
                puts "has view"
                #also run any pre query hook for view:
                #replacement_table.view.run_pre_query_hook(engine.identity, replacement_table, engine.db, frm.parameters)
                Jungle::Db::Model::View.run_pre_query_hook(replacement_table.view, engine.identity, replacement_table, engine.db, frm.parameters)
                #table_replacement_text = replacement_table.view.build_call(engine.identity, replacement_table, frm.parameters)
                table_replacement_text = Jungle::Db::Model::View.build_call(replacement_table.view, engine.identity, replacement_table, frm.parameters)
              else
                puts "no view"
                table_replacement_text = "#{replacement_table.table_name}"
              end
              frm.table_replacement_text = table_replacement_text
            end
          end
        end
      end

      def self.generate_sql(select, from, where, sort, entity, apply_limit_offset_to_sql)
        lf = "\n"
        indent_comma = ",\n  "
        sql = "select\n  #{select.join(indent_comma)}\nfrom #{from.join(lf)}"
        if where and where.length > 0
          sql = sql + "\nwhere\n  #{where.join(' ')}"
        end
        if sort and sort.length > 0
          sql =  sql + "\norder by\n  #{sort.join(', ')}"
        end
        if apply_limit_offset_to_sql
          if entity.limit and entity.limit > 0
            sql = sql + "\nlimit #{entity.limit}"
          end
          if entity.offset and entity.offset > 0
            sql = sql + "\noffset #{entity.offset}"
          end
        end
        sql
      end
    end
  end
end
