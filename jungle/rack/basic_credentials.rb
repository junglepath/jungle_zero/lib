# basic_credentials.rb
# Add basic auth credentials into Rack env.
# Return 401 Unauthorized if no credentials are passed.
require 'rack'
require 'rack/auth/abstract/handler'
require 'rack/auth/abstract/request'

module Jungle
  module Rack
    module BasicCredentials
      # Rack::Auth::Basic implements HTTP Basic Authentication, as per RFC 2617.
      #
      # Initialize with the Rack application that you want protecting,
      # and a block that checks if a username and password pair are valid.
      #
      # See also: <tt>example/protectedlobster.rb</tt>
      class Challenge < ::Rack::Auth::AbstractHandler
        def initialize(app, realm=nil, issue_challenge=true, &authenticator)
          @issue_challenge = issue_challenge
          super(app, realm, &authenticator)
        end

        def call(env)
          #puts "realm: #{realm}."

          auth = Challenge::Request.new(env)

          if @issue_challenge
            return unauthorized unless auth.provided?
            return bad_request unless auth.basic?
          end

          if auth.provided? and auth.basic?
            env['REMOTE_USER'] = auth.username
            env['REMOTE_PASSWORD'] = auth.password
          else
            env['REMOTE_USER'] = nil
            env['REMOTE_PASSWORD'] = nil
          end

          @app.call(env)
        end

        private

        def challenge
          'Basic realm="%s"' % realm
        end

        class Request < ::Rack::Auth::AbstractRequest
          def basic?
            "basic" == scheme
          end

          def credentials
            @credentials ||= params.unpack("m*").first.split(/:/, 2)
          end

          def username
            credentials.first
          end

          def password
            credentials.last
          end
        end
      end

      class NoChallenge < Challenge
        def initialize(app, realm=nil, issue_challenge=false, &authenticator)
          super(app, realm, issue_challenge, &authenticator)
        end
      end
    end
  end
end
