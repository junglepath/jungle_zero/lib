# rack_json_body_parser.rb -- mod of code from rack-contrib. This mod allows sybolizing the hash key names by passing in a second parameter to initialize.
require 'rack'
require 'jungle/json'

module Jungle
  module Rack
    # A Rack middleware for parsing POST/PUT body data when Content-Type is
    # not one of the standard supported types, like <tt>application/json</tt>.
    class JsonBodyParser
      # Constants
      CONTENT_TYPE = 'CONTENT_TYPE'.freeze
      POST_BODY = 'rack.input'.freeze
      FORM_INPUT = 'rack.request.form_input'.freeze
      FORM_HASH = 'rack.request.form_hash'.freeze

      # Supported Content-Types
      APPLICATION_JSON = 'application/json'.freeze

      def initialize(app, symbolize_names=false)
        @app = app
        @symbolize_names = symbolize_names
      end

      def call(env)
        if ::Rack::Request.new(env).media_type == APPLICATION_JSON && (body = env[POST_BODY].read).length != 0
          puts "rack body: #{body}."
          env[POST_BODY].rewind # somebody might try to read this stream
          #env.update(FORM_HASH => JSON.parse(body, :symbolize_names=>@symbolize_names), FORM_INPUT => env[POST_BODY])
          env.update(FORM_HASH => Jungle::Json.parse(body, :symbolize_names=>@symbolize_names), FORM_INPUT => env[POST_BODY])
        end
        @app.call(env)
      end
    end
  end
end
