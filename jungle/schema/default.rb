module Jungle
  module Schema
    module Default

      def self.hash()
        s = {}
        s[:role] = role
        s[:schema_info] = schema_info
        s[:user] = user
        s[:user_role] = user_role
        s
      end

      def self.named_column_sets()
        ncs = {}
        ncs[:audit_user] = {
          name: :audit_user,
          columns: [
            [:created_by_user_id, :foreign_key, :user],
            [:created_at, :timestamp, :default, 'now()'],
            [:updated_by_user_id, :foreign_key, :user],
            [:updated_at, :timestamp, :default, 'now()']
          ]}
        ncs
      end

      def self.array()
        hash.values
      end

      def self.role()
        {
          name: :role,
          description: "a named security principal -- set of permissions and restrictions",
          columns: [
            [:id, :primary_key],
            [:role, :string],
            [:description, :string],
            [:enabled, :boolean],
            [:named_column_set, :audit_user]
          ],
          view: nil
        }
      end

      def self.schema_info()
        {
          name: :schema_info,
          description: "Table used by the Sequel Ruby gem to track database migration versions.",
          columns: [
            [:version, :integer, :primary_key]
          ],
          view: nil
        }
      end

      def self.user()
        {
          name: :user,
          description: "authenticated user of the system",
          columns: [
            [:id, :primary_key],
            [:username, :string, :unique, :not_null, :alternate_key],
            [:password_hash, :string, :not_null, :secure, :calculated, :desc, "See password_hash.rb for details."],
            [:default_role, :string, :not_null],
            [:name, :string, :desc, "Your human name! :)"],
            [:email, :string],
            [:mobile, :string],
            [:organization, :string, :secure],
            [:notes, :string, :desc, "For any misc. notes about this user including addition email addresses, phone numbers, etc."],
            [:enabled, :boolean, :default, false],
            [:sms_verification_code, :string, :secure, :desc, "phone verification/sign-in code."],
            [:activation_key, :string, :secure],
            [:password_reset_code, :string, :secure],
            [:named_column_set, :audit_user]
          ],
          plural: "users",
          view: nil
        }
      end
      
      def self.user_role()
        {
          name: :user_role,
          description: "associates a user with a role.",
          columns: [
            [:user_id, :foreign_key, :user, :primary_key],
            [:role_id, :foreign_key, :role, :primary_key],
            [:enabled, :boolean, :default, false],
            [:named_column_set, :audit_user]
          ]
        }
      end
    end
  end
end
