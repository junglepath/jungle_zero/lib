module Jungle
  module Schema
    module Filter
      #
      # queries use allowed tables + columns
      # generated sql applies substitutes to tables that have replacements (such as other tables, views, functions...)
      # replacements column names must match original column names for allowed columns.
      #
      def self.allowed(models_in, schema_filters)
        models_out = {} # {user: {columns: {id: true, etc: true}}, role: {columns: {...}}}
        allows = schema_filters[:allow] || []
        denies = schema_filters[:deny] || []
        models_in.each do |m|
          at, acs = match_model(m, allows)

          next unless at

          dt, dcs = match_model(m, denies)

          columns_out = nil
          acs.each do |ac|
            columns_out ||= {}
            columns_out[ac] = ac unless dcs && dcs.include?(ac)
          end unless dt && (dcs == nil || dcs == []) # deny table without specifying columns denies all columns

          models_out[at] = columns_out if columns_out
        end
        models_out
      end

      def self.substitutes(table_filters)
        subs = {}
        table_filters.each do |tf|
          subs[tf[:table_name]] = tf[:replacement]
        end
        subs
      end

      def self.match_model(model, filters)
        table = nil
        columns = nil
        filter = nil
        filters.each do |f|
          filter = f if match_name(f[:table], model.name)
          break if filter
        end
        if filter
          table = model.name
          columns = match_columns(filter[:columns], model.columns)
        end
        return table, columns
      end

      def self.match_columns(filter, columns)
        return columns.values.map{|c| c.name} unless filter # if nil, allow all columns as a default.
        return columns.values.map{|c| c.name} if filter == /./
        return columns.values.select{|c| filter =~ c.name.to_s}.map{|c| c.name} if filter.class == Regexp
        return columns.values.select{|c| filter == c.name.to_s}.map{|c| c.name} if filter.class == String
        return columns.values.select{|c| filter == c.name}.map{|c| c.name} if filter.class == Symbol
        if filter.class == Array
          names = []
          columns.values.each do |c|
            filter.each do |f|
              if match_name(f, c.name)
                names << c.name
                break
              end
            end
          end
          return names
        end
        raise "Invalid columns filter: #{filter}"
      end

      def self.match_name(filter_expression, value)
        return true if filter_expression.class == Regexp && filter_expression =~ value.to_s
        return true if filter_expression.class == String && filter_expression == value.to_s
        return true if filter_expression == value
        false
      end

      def self.sample_schema_filters()
        {
          allow: [
            {table: "zztop"},
            {table: /./, columns: nil},
            {table: :contact, columns: []},
            {table: :user, columns: /./},
            {table: :opportunity, columns: :name}
          ],
          deny: [
            {table: /^temp_/},
            {table: :user, columns: [:secret]}
          ]
        }
      end

      def self.sample_table_filters()
        # the :user role my have table filters that look like this:
        [
					{table_name: :user, replacement: :filter_user},
					#{table_name: :filter_user, replacement: :filter_user},
					{table_name: :group, replacement: :filter_group},
					#{table_name: :filter_group, replacement: :filter_group},
					{table_name: :group_available_member, replacement: :filter_group_available_member},
					#{table_name: :filter_group_available_member, replacement: :filter_group_available_member},
					{table_name: :question, replacement: :filter_question},
					#{table_name: :filter_question, replacement: :filter_question},
					{table_name: :quiz, replacement: :filter_quiz},
					{table_name: :quiz_available_question, replacement: :filter_quiz_available_question},
					#{table_name: :filter_quiz_available_question, replacement: :filter_quiz_available_question},
					#{table_name: :filter_quiz, replacement: :filter_quiz},
					{table_name: :practice, replacement: :filter_practice},
					{table_name: :practice_view, replacement: :filter_practice}
					#{table_name: :filter_practice, replacement: :filter_practice}
				]
      end
    end
  end
end

#  tables:
#
#  user(id, name, created_by_id, location_id, previous_location_id, car_id)
#  phone(id, user_id[fk], number)
#  user_role(user_id, role_id, created_by_id)
#  role(id, name, created_by_id)
#  location(id, name, created_by_id)
#  car(id, name, year)
#
#  queries:
#  
#  user(
#    id,
#    name,
#    created_at,
#    location[location_id](id, name),
#    previous_location[previous_location_id](id, name),
#    user_role(role(id, name)),
#    phone(id, number),
#    car(id, name, year),
#    created_by_user(id, name)
#  )(name ~= 'mik%')
#  
#  {table: user,
#  parts: [
#    [:id, :value],
#    [:name, :value],
#    [:create_at, :value],
#    [:location, :fk, :location_id, {table: location, parts: [[:id, :value], [:name, :value]]}],
#    [:previous_location, :fk, :previous_location_id, :location]
#  }
#
