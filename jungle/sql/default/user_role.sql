select
  c.id,
  c.role,
  c.enabled and b.enabled as enabled
from "user" a 
join "user_role" b on b.user_id = a.id
join "role" c on c.id = b.role_id
where user_id = ?