module Jungle
  module Sql
    module Helpers
      def self.load(filename, sql_root:nil, sub_folder:'default')
        sql_root ||= __dir__
        file = File.join(sql_root, sub_folder, filename)
        puts "file: #{file}."
        sql = File.read(file)
        puts "sql: #{sql}."
        sql
      end
    end
  end
end
